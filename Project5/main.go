package main

import (
    "fmt"
    "bigint/bigint"
)
func main(){
    
	a, err :=bigint.NewInt("988847123412385995937737458959")
if err != nil {
    panic(err)
}
fmt.Println(a)
b, err :=bigint.NewInt("21231231231231231231231231233")
if err != nil {
    panic(err)
}
//fmt.Println(b)
// err = a.Set("15") // a = "15"
// if err != nil {
//     panic(err)
// }
err = b.Set("10") // b = "1"
if err != nil {
    panic(err)
}
fmt.Println(b)
// fmt.Println(bigint.Add(a,b))
// fmt.Println(bigint.Sub(a,b))
// fmt.Println(bigint.Multiply(a,b))
// fmt.Println(bigint.Sub(a,b))
// fmt.Println(bigint.Mod(a,b))
fmt.Println(b.Abs())
//c:=bigint.Add(a, b) // c = "988847123412385995937737458960"
//d:=bigint.Sub(a, b) // d = "988847123412385995937737458958"
//e:=bigint.Multiply(a, b) // e = "988847123412385995937737458959"
//f:=bigint.Mod(a, b) // f = "0" // Mod(a,b) works for only positive numbers
//fmt.Println(c) 
//fmt.Println(d)
//fmt.Println(e)
//fmt.Println(f)
}

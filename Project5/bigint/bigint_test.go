package bigint

import (
	"testing"
	"fmt"
    )

func TestAdd(t *testing.T){
	a,_:=NewInt("12")
	b,_:=NewInt("10")
	want,_:=NewInt("22")
	got:=Add(a,b)
	if want!=got{
		t.Errorf("got %q wanted %q",got.value, want.value)}
}
func TestAddTD(t *testing.T){
	tests := []struct{
		label string
		a,b string
		wanted string
	}{
		{"Test 1","120","211","331"},
		{"Test 2","-1","1","0"},
		{"Test 3", "25469874154789562458795482315445","48759587469854128745123654","25469922914377032312924227439099"},
		{"Test 4","25469874154789562458795482315445","-48759587469854128745123654","25469825395202092604666737191791"},
		{"Test 5","-120","200","80"},
		{"Test 6","200","-300","-100"},
		{"Test 7","-10","-20","-30"},
		{"Test 8","50","50","100"},
		{"Test 9","10","1000","1010"},
		
	}
	for _,tt:=range tests{
		t.Run(tt.label,func(t *testing.T){
			a,_:=NewInt(tt.a)
			b,_:=NewInt(tt.b)
			ans:=Add(a,b)
            testCaseTextOut:= fmt.Sprintf("Add(%s,%s)",a.value,b.value)
			if ans.value != tt.wanted {
				t.Errorf("Test: %s | Result %s | Expected %s",testCaseTextOut,ans.value,tt.wanted)
			}
		})
	}
}

func TestNewInt(t *testing.T){
	tests:=[]struct{
		label string
		a string
		wanted string
		err error
	}{
		{"Test 1","2541","2541",nil},
		{"Test 2","+210","210",nil},
		{"Test 3","-12","-12",nil},
		{"Test 4","000154","154",nil},
		{"Test 5","0000","0",nil},
		{"Test 6","231g","",ErrorMessage},
	}

	for _,tt:=range tests{
         
		t.Run(tt.label,func(t *testing.T){

		a,err:=NewInt(tt.a)
	//	fmt.Println(err,tt.err)
		if err!=tt.err{
			t.Errorf("Error: %e",err)
		}else if tt.wanted!=a.value{
			t.Errorf("got %s wanted %s",a.value,tt.wanted)
		}	
		})

		
	}
}

func TestSet(t *testing.T){
	tests:=[]struct{
		label string
		num string
		wanted string
		err error
	}{
		{"Test 1","10","10",nil},
		{"Test 2","2a","",ErrorMessage},
	}
	for _,tt:=range tests{

		t.Run(tt.label,func(t *testing.T){
			a,_:=NewInt("126")
			err:=a.Set(tt.num)
			if err!=tt.err{
				t.Errorf("Expected %v Got %v",tt.err,err)
			}

		})

	}
}

func TestSub(t *testing.T){
	tests := []struct{
		label string
		a,b string
		wanted string
	}{
		{"Test 1","150","100","50"},
		{"Test 2","-1","1","-2"},
		{"Test 3", "25469874154789562458795482315445","48759587469854128745123654","25469825395202092604666737191791"},
		{"Test 4","25469874154789562458795482315445","-48759587469854128745123654","25469922914377032312924227439099"},
		{"Test 5","120","200","-80"},
		{"Test 6","-200","-300","100"},
		{"Test 7","-10","-20","10"},
		{"Test 8","1","1","0"},
		
	}
	for _,tt:=range tests{
		t.Run(tt.label,func(t *testing.T){
			a,_:=NewInt(tt.a)
			b,_:=NewInt(tt.b)
			ans:=Sub(a,b)
            testCaseTextOut:= fmt.Sprintf("Sub(%s,%s)",a.value,b.value)
			if ans.value != tt.wanted {
				t.Errorf("Test: %s | Result %s | Expected %s",testCaseTextOut,ans.value,tt.wanted)
			}
		})
	}

}

func TestMultiply(t *testing.T){
	tests := []struct{
		label string
		a,b string
		wanted string
	}{
		{"Test 1","0","100","0"},
		{"Test 2","-1","1","-1"},
		{"Test 3", "25469874154789562458795482315445","48759587469854128745123654","1241900556696638667648164824882749575236745628536759036030"},
		{"Test 4","25469874154789562458795482315445","-48759587469854128745123654","-1241900556696638667648164824882749575236745628536759036030"},
		{"Test 5","-10","-5","50"},
		
	}
	for _,tt:=range tests{
		t.Run(tt.label,func(t *testing.T){
			a,_:=NewInt(tt.a)
			b,_:=NewInt(tt.b)
			ans:=Multiply(a,b)
            testCaseTextOut:= fmt.Sprintf("Multiply(%s,%s)",a.value,b.value)
			if ans.value != tt.wanted {
				t.Errorf("Test: %s | Result %s | Expected %s",testCaseTextOut,ans.value,tt.wanted)
			}
		})
	}

}


func TestMod(t *testing.T){
	tests := []struct{
		label string
		a,b string
		wanted string
	}{
		{"Test 1","20","100","20"},
		{"Test 2","30","8","6"},
		{"Test 3", "25469874154789562458795482315445","48759587469854128745123654","11082386439184007670906621"},
		{"Test 4","988847123412385995937737458960","1","0"},
		
	}
	for _,tt:=range tests{
		t.Run(tt.label,func(t *testing.T){
			a,_:=NewInt(tt.a)
			b,_:=NewInt(tt.b)
			ans:=Mod(a,b)
            testCaseTextOut:= fmt.Sprintf("Mod(%s,%s)",a.value,b.value)
			if ans.value != tt.wanted {
				t.Errorf("Test: %s | Result %s | Expected %s",testCaseTextOut,ans.value,tt.wanted)
			}
		})
	}

}

func TestAbs(t *testing.T){
	tests:=[]struct{
		label string
		num string
		wanted string
	}{
		{"Test 1","10","10"},
		{"Test 2","-10","10"},
		{"Test 3", "0","0"},
	}
	for _,tt:=range tests {
		t.Run(tt.label,func (t *testing.T){
			a,_:=NewInt(tt.num)
			 testCaseTextOut:= fmt.Sprintf("a.Abs(%s)",a.Abs())
			if a.Abs().value != tt.wanted {
				t.Errorf("Test: %s | Result %s | Expected %s",testCaseTextOut,a.value,tt.wanted)
			}

		})
	}
}


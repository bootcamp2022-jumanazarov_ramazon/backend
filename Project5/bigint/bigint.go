package bigint

import (
	"errors"
	"fmt"
	"strconv"
)

var ErrorMessage =errors.New("please , input only numbers")
type BigInt struct {
	value string
}
 
func validate(num string) (string,bool){
   error:=false
  var ishora string
  if num[0]==45{
      ishora="-"
      num=num[1:]
  }
  if num[0]==43{
      num=num[1:]
  }
  
  for i :=range num {
      if num[i]<48||num[i]>57{
          error=true
      }
  }
  for num[0]==48&&len(num)>1{
      num=num[1:]
  }
  
  if ishora=="-" && num!="0"{
      num="-"+num
  }
 return num,error

 }

func NewInt(num string)(BigInt,error){
	numbers,err:=validate(num)
	
	
	if err{
		return BigInt{},ErrorMessage
	} else {
		return BigInt{numbers},nil}
}

func (z *BigInt) Set(num string)error{
	number,err:= NewInt(num)
	if err==nil{
		*z=number
		return nil
	} else{
		return err
	}
}
func firstNumisBigger(num1,num2 BigInt) (isBigger,equal bool){
    length1:=len(num1.value)
	length2:=len(num2.value)
	var result bool
	var count int
	if length1>length2{
		result=true
	} else if length2>length1{
		result=false
	}else{ 
		for i:=0;i<length1;i++{
			if num1.value[i]>num2.value[i]{
				result=true
				break
			}else if num1.value[i]<num2.value[i]{
				result=false
				break
			}else{
				count++
				if count==length1{
					equal=true
				}
			}

		}

	}
	if string(num1.value[0])=="-" && string(num2.value[0])!="-"{
		result=false
		equal=false
	}
	//fmt.Println(string(num1.value[0]))

 return result,equal
}

func sub(a,b BigInt)BigInt{
	c:=""
	var k,delta,aValue,bValue int
     aLength:=len(a.value)
	 bLength:=len(b.value)
	for i:=0;i<aLength;i++{
     aValue=int(a.value[aLength-i-1]-48)
	 if i<bLength{
	 bValue=int(b.value[bLength-i-1]-48)}else{
		 bValue=0
	 }
	 k=aValue-bValue-delta
	 if k<0{
		 k+=10
		 delta=1
	 }else{
		 delta=0
	 }
	 //fmt.Println(k,delta,string(k))
	 c=strconv.Itoa(int(k))+c
}   
   for c[0]=='0'{
      c=c[1:]
   }
  result:=BigInt{c}
   return result

}
func Sub(a,b BigInt)BigInt{
	var ishoraA,ishoraB string
    if a.value[0]==45{
		ishoraA="-"
		a.value=a.value[1:]
	}
	if b.value[0]==45{
		ishoraB="-"
		b.value=b.value[1:]
	}
	if ishoraA!="-"&&ishoraB!="-"{


	firstIsbig,equal:=firstNumisBigger(a,b)
	if equal{
      return BigInt{"0"}
	} else if firstIsbig{
		return sub(a,b)
	}else{  
		if sub(b,a).value==""{
			return BigInt{"0"}
		}
		return BigInt{"-"+sub(b,a).value}
	}
}else if ishoraA=="-"&&ishoraB!="-"{
	result:=Add(a,b)
	result.value="-"+result.value
	return result
}else if ishoraA!="-"&&ishoraB=="-"{
	result:=Add(a,b)
	return result
}else{
	return Sub(b,a)
}
}

func mulToAloneInt(a BigInt, m int) BigInt{
   length:=len(a.value)
	c:=""
	var k,delta,aValue int

	for i:=0;i<length;i++{
		
     aValue=int(a.value[length-i-1]-48)
	 
	 
	 k=(delta+aValue*m)%10
	 delta=(delta+aValue*m)/10
	 //fmt.Println(k,delta,string(k))
	 c=strconv.Itoa(int(k))+c
}
 if delta != 0 {
	 c=strconv.Itoa(int(delta))+c
 }
  result:=BigInt{c}
   return result
   
}
func Multiply(a,b BigInt) BigInt{
	var ishoraA,ishoraB string
    result:=BigInt{"0"}	
    if a.value[0]==45{
		ishoraA="-"
		a.value=a.value[1:]
	}
	if b.value[0]==45{
		ishoraB="-"
		b.value=b.value[1:]
	}
	//fmt.Println(ishoraA,a,ishoraB,b)
	if (ishoraA!="-" &&ishoraB!="-")||(ishoraA=="-"&&ishoraB=="-"){
	 length:=len(b.value)
	 //fmt.Println(length)
	
	for i:=0;i<length;i++{
        dif:=""
		for j:=0;j<i;j++{
			dif+="0"
		}
		m:=int(b.value[length-i-1]-48)

		res,err:=NewInt(string(mulToAloneInt(a,m).value)+dif)
		if err!=nil{
			fmt.Println(err)
		}
		result=Add(res,result)
	}
	

}else{
	result=Multiply(a,b)
	result.value="-"+result.value
}
return result
}

func Mod(a,b BigInt)BigInt{
	//result:=BigInt{"0"}
	k:=len(a.value)
	dif:=""
     for len(b.value)<k-1{
		 dif = ""
		 for j:=0;j<len(a.value)-len(b.value)-1;j++{
			 dif+="0"
		 }
		 c,err:=NewInt(b.value+dif)
		 //fmt.Println(c)
		 if err==nil{
		 a=Sub(a,c)
		 k=len(a.value)
		//   fmt.Println(a,k)
		//  var n int
		//  fmt.Scanf("%d",n)
		 }
	 }

	for {
		firstBigger,equal:=firstNumisBigger(a,b)
		//fmt.Println(firstBigger,equal)
		if equal{
			a= BigInt{"0"}
			break;
		}else if firstBigger{

			a=Sub(a,b)
		}else{
			break
		}
		
	}
	return a
}

func (x *BigInt) Abs()BigInt{
	if string(x.value[0])=="-" || string(x.value[0])=="+"{
		return BigInt{
			value : x.value[1:],
		}
	}
	return BigInt{
		value:x.value,
	}

}

func Add(a,b BigInt)(res BigInt){
	aIsPositive:=a.value[0]!='-'
	bIsPositive:=b.value[0]!='-'
	if aIsPositive && bIsPositive{
		var length int
	aLength:=len(a.value)
	bLength:=len(b.value)
    if aLength>bLength{
		length=aLength
	}else{
		length=bLength
	}
	c:=""
	var k,delta,aValue,bValue uint8

	for i:=0;i<length;i++{
		if i<aLength{
     aValue=a.value[aLength-i-1]-48}else{
     aValue=0}
	 if i<bLength{
	 bValue=b.value[bLength-i-1]-48}else{
		 bValue=0
	 }
	 k=(delta+aValue+bValue)%10
	 delta=(delta+aValue+bValue)/10
	 //fmt.Println(k,delta,string(k))
	 c=strconv.Itoa(int(k))+c
   }
 if delta != 0 {
	 c=strconv.Itoa(int(delta))+c
 }
  result:=BigInt{c}
   return result
	}else if !aIsPositive && bIsPositive {
		a.value=a.value[1:]
		res:=Sub(b,a)
		return res
	}else if aIsPositive && !bIsPositive {
		b.value=b.value[1:]
		res:=Sub(a,b)
		//fmt.Println(res)
		return res

	}else{
		a.value=a.value[1:]
		b.value=b.value[1:]
		res=Add(a,b)
		res.value="-"+res.value
		return res
	}
}
#! /usr/bin/bash
# removeDuplicates.sh
FILES="$(ls)" 

for file1 in $FILES; do
    for file2 in $FILES; do
        #echo "checking $file1 and $file2"
        if [[ "$file1" != "$file2" && -e "$file1" && -e "$file2" ]]; then
            if diff "$file1" "$file2" > /dev/null 
            then
               # echo "$file1 and $file2 are duplicates"
                rm -v "$file2"
            fi
        fi
    done
done

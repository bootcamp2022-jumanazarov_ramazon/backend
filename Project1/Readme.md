# Task 
> Write a bash script(remove_duplicates.sh) that removes duplicate files in a folder.


# My Script

```bash
#! /usr/bin/bash
# removeDuplicates.sh
FILES="$(ls)" 

for file1 in $FILES; do
    for file2 in $FILES; do
        #echo "checking $file1 and $file2"
        if [[ "$file1" != "$file2" && -e "$file1" && -e "$file2" ]]; then
            if diff "$file1" "$file2" > /dev/null 
            then
               # echo "$file1 and $file2 are duplicates"
                rm -v "$file2"
            fi
        fi
    done
done

```

## Example 

1) In my directory has 3 files

![boshlang'ich holat](./Images/image1.png)

2) I copied car.jpg 4 times  
 

![copydan keyin](./Images/image2.png)

3) I executed my remove_dublicates.sh file

![remove_dublicate.sh executed](./Images/image3.png)

4) After execution we can see a file 

![result](./Images/image4.png)
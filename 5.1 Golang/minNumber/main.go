package main
import "fmt"

func main() {
    var n int
   fmt.Scanf("%d",&n)
   fmt.Println(DisplayMinimumNumberFunction(n))
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) ( string) {
     
    if n==2 {
		return "min(int, int)"
	}else {
		return "min(int, "+DisplayMinimumNumberFunction(n-1)+")"
	}
    
}
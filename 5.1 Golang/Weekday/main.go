package main

import (
    "fmt"
    "time"
)

func main() {
    dobStr := "27.12.1998" // Replace this date with your birthday
    givenDate, err := time.Parse("02.01.2006", dobStr)
    if err != nil {
        panic(err)
    }
    fmt.Printf("%s is %s\n", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string){
	// first Way
    ////////////////////// 
    // return  date.Weekday().String()
    ////////////////////////

// 	// second Way
	year := int(date.Year())
	month := int(date.Month())
	dayOfMonth := int(date.Day())
  var daysInMonths int
    
   switch month {
   case 2:
	daysInMonths=31
   case 3:
	daysInMonths=59
   case 4:
	daysInMonths = 90
   case 5:
	daysInMonths=120
   case 6:
	daysInMonths=151
   case 7:
	daysInMonths=181
   case 8:
	daysInMonths = 212
   case 9:
	daysInMonths = 243
   case 10:
	daysInMonths = 273
   case 11:
	daysInMonths = 304
   case 12:
	daysInMonths = 334	
   default:
	 daysInMonths=0									
}
 day :=((year-1)*365+daysInMonths+dayOfMonth)
 day = (day+(year-1)/4 - (year-1)/100 +(year-1)/400)

 isLeapYear := year%4==0 && year%100!=0 
 if year%400==0 {isLeapYear=true}
 if isLeapYear {
	 if month>2||(month==2 && dayOfMonth==29){day++}
 }
 day%=7
 
 switch day {
 case 0:
	weekday = "Sunday"
 case 1:
	weekday = "Monday"
 case 2:
	weekday = "Tuesday"
 case 3:
	weekday = "Wednesday"	
 case 4:
	weekday = "Thursday"
 case 5:
	weekday = "Friday"
 default:
	weekday="Saturday"					
 }
 return weekday

}
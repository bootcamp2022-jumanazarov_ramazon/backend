package main

import  ("fmt" 
         "math"  )


func main() {
    fmt.Println(MySquareRoot(10, 10))
}

func MySquareRoot(num, precision uint) (result float64) {
    // DO NOT USE math.Sqrt!
   var start uint= 0
	end := num
    var mid uint
	for start <= end {
        mid =(start+end)/2
		if (mid*mid==num){
			result = float64(mid)
			break;
		}
		if(mid*mid<num){
			start=mid+1
			result = float64(mid)
		} else {
			end=mid-1
		}
	}
	 var delta =1/math.Pow(10,float64(precision))
	 fmt.Println(float64(precision))
	// fmt.Println(delta,precision)
	// delta :=1.0
	// for i  := 1;i<=int(precision);i++{
	// 	delta/=10
	// }
	fmt.Println(delta)
	for result*result <= float64(num) {
		result+=delta
	}

    result -=delta

    return result
}
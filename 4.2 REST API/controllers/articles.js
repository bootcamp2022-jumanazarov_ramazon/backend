import {v4 as uuidv4} from 'uuid';
import datetime from 'node-datetime';

let articles = [
    {
        id:uuidv4(),
        title:" IT in Uzbekistan",
        text: 'Some information about IT technology in Uzbekistan  ',
        author: "Author1",
        publishedTime: '2022-05-05 09:26:37'
    },
    {
        id:uuidv4(),
        title:"Football News",
        text: 'Some information about Football news ',
        author: "Author2",
        publishedTime: '2022-05-07 21:15:24'
    },
    {
        id:uuidv4(),
        title:"Medical advices for summer days",
        text: 'Some advices by doctors  ',
        author: "Author3",
        publishedTime: '2022-05-08 14:45:30'
    },
]


export const getArticles = (req, res)=>{
    res.send(articles)

}

export const createArticle = (req,res)=>{
    const {title,text,author} = req.body
    const article ={
        id:uuidv4(),
        title,
        text,
        author,
        publishedTime:datetime.create().format('Y-m-d H:M:S')
    }
    articles.push(article)
    res.send(article)
}

export const getArticle = (req,res)=>{
    const {id}=req.params;
   const article = articles.find(art => art.id === id);
    res.send(article)
}
export const deleteArticle = (req,res)=>{
  const {id}=req.params;
  articles = articles.filter(art=>art.id !== id)
  res.send(`Article with the id: ${id} was deleted`);
}

export const updateArticle = (req,res)=>{
 
    const {id} = req.params;
    let {title,text,author} = req.body;
    const article = articles.find(art=> art.id === id)
    if(title) article.title = title;
    if(author) article.author = author;
    if(text) article.text = text;
    article.publishedTime = datetime.create().format('Y-m-d H:M:S');
    res.send(article);


}


import express from "express";
import bodyParser from "body-parser";

import articlesRoutes from './routes/articles.js'

const app = express();
const PORT = 3000;
app.use(bodyParser.json())

app.use('/articles',articlesRoutes);



app.get('/', (req,res)=>{
    res.status(200).send("<h1>Hello from homepage</h1>");
})


app.listen(PORT,()=>{
    console.log(`Server is running on ${PORT} port `)
})
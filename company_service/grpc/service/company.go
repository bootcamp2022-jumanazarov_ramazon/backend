package service

import (
	"context"
	"company_service/config"
	pb "company_service/genproto/company_service"
	"company_service/pkg/logger"
	"company_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type companyService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedCompanyServiceServer
}

func NewCompanyService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *companyService {
	return &companyService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *companyService) Create(ctx context.Context, req *pb.CreateCompanyRequest) (*pb.CompanyId, error) {
	id, err := s.strg.Company().Create(ctx, req)
	if err != nil {
		s.log.Error("CreateCompany", logger.Any("req", req), logger.Error(err))
		return nil, err
	}

	return &pb.CompanyId{
		Id:   id,
	}, nil
}

func (s *companyService) GetAll(ctx context.Context, req *pb.CompanyGettAllRequest) (*pb.CompanyGettAllResponse, error) {
	resp, err := s.strg.Company().GetAll(ctx, req)
	if err != nil {
		s.log.Error("GetAllCompany", logger.Any("req", req), logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func(s *companyService) Get(ctx context.Context, req *pb.CompanyId)(*pb.Company,error){

	resp,err := s.strg.Company().Get(ctx, req)

	if err != nil {
		s.log.Error("Get Company", logger.Any("req", req), logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (s *companyService) Update(ctx context.Context, req *pb.Company)(*pb.CompanyId,error){
	ID,err := s.strg.Company().Update(ctx, req)
	if err != nil {
		s.log.Error("GetAllCompany", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return &pb.CompanyId{
		Id: ID,
		}, nil
}

func (s *companyService) Delete(ctx context.Context,req *pb.CompanyId)(*emptypb.Empty,error){

	err := s.strg.Company().Delete(ctx,req)
	
	if err != nil {
		s.log.Error("Delete Company", logger.Any("req", req), logger.Error(err))
		return &emptypb.Empty{}, err
	}

	return &emptypb.Empty{}, nil
}
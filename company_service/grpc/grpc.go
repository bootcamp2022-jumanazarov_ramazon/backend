package grpc

import (
	"company_service/config"
	"company_service/genproto/company_service"
	"company_service/grpc/service"
	"company_service/pkg/logger"
	"company_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	company_service.RegisterCompanyServiceServer(grpcServer, service.NewCompanyService(cfg, log, strg))

	reflection.Register(grpcServer)
	return
}
package postgres

import (
	"context"
	"fmt"
	"company_service/pkg/helper"
	"company_service/storage"

	pb "company_service/genproto/company_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type companyRepo struct {
	db *pgxpool.Pool
}

func NewCompanyRepo(db *pgxpool.Pool) storage.CompanyI {
	return &companyRepo{
		db: db,
	}
}

func (r *companyRepo) Create(ctx context.Context, entity *pb.CreateCompanyRequest) (id string, err error) {
	query := `
		INSERT INTO company (
			id,
			name
		) 
		 VALUES ($1, $2)
	`

	id = uuid.New().String()

	_, err = r.db.Exec(
		ctx,
		query,
		id,
		entity.Name,
	)

	if err != nil {
		return "", fmt.Errorf("error while inserting company err: %w", err)
	}

	return id, nil
}

func (r *companyRepo) GetAll(ctx context.Context, req *pb.CompanyGettAllRequest) (*pb.CompanyGettAllResponse, error) {
	var (
		resp   pb.CompanyGettAllResponse
		err    error
		filter string
		params = make(map[string]interface{})
	)

	if req.Search != "" {
		filter += " AND name ILIKE '%' || :search || '%' "
		params["search"] = req.Search
	}

	countQuery := `SELECT count(1) FROM company WHERE true ` + filter

	q, arr := helper.ReplaceQueryParams(countQuery, params)
	err = r.db.QueryRow(ctx, q, arr...).Scan(
		&resp.Count,
	)

	if err != nil {
		return nil, fmt.Errorf("error while scanning count %w", err)
	}

	query := `SELECT
				id,
				name
			FROM company
			WHERE true` + filter

	query += " LIMIT :limit OFFSET :offset"
	params["limit"] = req.Limit
	params["offset"] = req.Offset

	q, arr = helper.ReplaceQueryParams(query, params)
	rows, err := r.db.Query(ctx, q, arr...)
	if err != nil {
		return nil, fmt.Errorf("error while getting rows %w", err)
	}

	for rows.Next() {
		var company pb.Company

		err = rows.Scan(
			&company.Id,
			&company.Name,
		)

		if err != nil {
			return nil, fmt.Errorf("error while scanning company err: %w", err)
		}

		resp.Companies = append(resp.Companies, &company)
	}

	return &resp, nil
}

func (r *companyRepo) Get(ctx context.Context,req *pb.CompanyId)(*pb.Company,error){

  query := `SELECT * FROM company  WHERE id=$1`
  var resp pb.Company
  err:=r.db.QueryRow(ctx,query,req.Id).Scan(&resp.Id,&resp.Name)
  if err != nil {
	return nil, fmt.Errorf("error while scanning err: %w",err)
  }
  
  return &resp,nil
  
}

func (r *companyRepo) Update(ctx context.Context,req *pb.Company)(id string, err error){
   query:=`UPDATE company SET name = $1 WHERE id = $2`

  raw, err:=r.db.Exec(ctx,query,req.Name,req.Id)

  if err != nil{
	return id,fmt.Errorf("error updating company err : %w",err)
  }
  if raw.RowsAffected()!=1{
	return id,fmt.Errorf("company not found")
  }
  return req.Id, nil
}

func(r *companyRepo) Delete(ctx context.Context,req *pb.CompanyId)(error){
	query:=`DELETE FROM company WHERE id = $1`

	raw, err:=r.db.Exec(ctx,query,req.Id)

	if err != nil{
		return fmt.Errorf("error while deleting company err: %w",err)
	}

	if raw.RowsAffected()!=1{
		return fmt.Errorf("company not found")
	}
	return nil
}
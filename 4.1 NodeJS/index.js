const http =require('http');
const fs = require('fs');
const path=require('path');
const server = http.createServer((req, res)=>{

  if (req.url ==="/")
  {
    req.url='/index.js'
  } 
  
fs.readFile(path.join(__dirname,`${req.url}`),'utf8',(err,data) =>{
    if (!err){
          res.writeHead(200, {'Content-Type':'text'})
          res.write(data);
          res.end();
    } 
    else {
      res.write(`There is no file with a name ${req.url.slice(1,)}`);
    res.end();}
  
})

})

const PORT =process.env.PORT || 3000;
server.listen(PORT,()=> console.log(`Server running on port:  ${PORT}`))
# TASK 
 >Create a node web server that reads files from its own source codes based on the entered URL path variable and displays their contexts to a web browser. The default page should display index.js context, other files should be accessed based on the URL path variable. If the file does not exist it should display There is no file with a name {path varibale}.


# My Answer

```bash
const http =require('http');
const fs = require('fs');
const path=require('path');
const server = http.createServer((req, res)=>{

  if (req.url ==="/")
  {
    req.url='/index.js'
  } 
  
fs.readFile(path.join(__dirname,`${req.url}`),'utf8',(err,data) =>{
    if (!err){
          res.writeHead(200, {'Content-Type':'text'})
          res.write(data);
          res.end();
    } 
    else {
      res.write(`There is no file with a name ${req.url.slice(1,)}`);
    res.end();}
  
})

})

const PORT =process.env.PORT || 3000;
server.listen(PORT,()=> console.log(`Server running on port:  ${PORT}`))

```


## Some Results

* The default page displays index.js context

![result1](./Images/result1.png)

* Context of the file on the URL path

![result2](./Images/result2.png)

* Case where file does not exist

![doesn't exist](./Images/result3.png)
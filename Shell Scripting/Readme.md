# TASK :

>  Write a bash script(screenshot.sh)  that screenshots your screen every given n seconds from terminal input. Name and store each screenshot file(ex: Screen Shot 2022-03-28 at 17.48.06.jpg) in a folder(ex: 2022-03-28) named based on a timestamp of each screenshot respectively. If a folder does not exist, your script should create it and store it into a folder that is named screenshot data placed next to the script file.



# My Script 

```bash
#! /usr/bin/bash
TODAY=$(date +"%Y-%m-%d")
if [ ! -d "$TODAY" ]
then
  mkdir -p "./screenshot data/$TODAY"
fi

read -p "Enter n : "  N

for (( ; ; ))
do  
   NOW=$(date  +"%H.%M.%S")
   NAME="Screen Shot $TODAY at $NOW"
   import -window root "./screenshot data/$TODAY/$NAME.jpg"
   sleep $N
done
```

#### I create some variables
 - N - for time between screenshots
 - TODAY - for Folder and Image name
 - NOW - for Image name
#### And I use   ***import -window root***   in order to get screenshots

#

## Some results 

#

### Before screenshot.sh run
   
   ![Boshlang'ich holat](./Images/image1.png)


### After screenshot.sh run

   ![Keyingi holat ](./Images/image2.png)

   We can see a folder  named  **2022-04-22**
### There are some screenshots in the inside of this folder    
  
   ![Screenshotlar](./Images/image3.png)


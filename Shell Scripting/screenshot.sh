#! /usr/bin/bash
TODAY=$(date +"%Y-%m-%d")
if [ ! -d "$TODAY" ]
then
  mkdir -p "./screenshot data/$TODAY"
fi

read -p "Enter n : "  N

for (( ; ; ))
do  
   NOW=$(date  +"%H.%M.%S")
#   echo " $NOW"
   NAME="Screen Shot $TODAY at $NOW"
   import -window root "./screenshot data/$TODAY/$NAME.jpg"
   sleep $N
done

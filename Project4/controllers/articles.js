import {v4 as uuidv4} from 'uuid';

let articles = [
    {
        id:uuidv4(),
        title:" IT in Uzbekistan",
        article: 'Some information about IT technology in Uzbekistan  ',
        publishedAt: '2022-05-05 09:26:37'
    },
    {
        id:uuidv4(),
        title:"Football News",
        article: 'Some information about Football news ',
        publishedAt: '2022-05-07 21:15:24'
    },
    {
        id:uuidv4(),
        title:"Medical advices for summer days",
        article: 'Some advices by doctors  ',
        publishedAt: '2022-05-08 14:45:30'
    },
]
export const getArticles = (req, res)=>{
    res.send(articles)

}

export const createArticle = (req,res)=>{
    const {title,text,author} = req.body
    const article ={
        id:uuidv4(),
        title,
        article,
        publishedAt
    }
    articles.push(article)
    res.send(article)
}
export const getArticle = (req,res)=>{
    const {id}=req.params;
   const article = articles.find(art => art.id === id);
    res.send(article)
}
export const deleteArticle = (req,res)=>{
  const {id}=req.params;
  articles = articles.filter(art=>art.id !== id)
  res.send(`Article with the id: ${id} was deleted`);
}

export const updateArticle = (req,res)=>{
 
    const {id} = req.params;
    let {title,article,publishedAt} = req.body;
    const pubArticle = articles.find(art=> art.id === id)
    if(title) pubArticle.title = title;
    if(text) pubArticle.article = article;
    pubArticle.publishedAt=publishedAt;
    res.send(pubArticle);

}

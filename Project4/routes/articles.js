import express from "express";
const router = express.Router();
import {getArticles,
    getArticle,
    createArticle,
    deleteArticle,
    updateArticle} from '../controllers/articles.js'

router.get('/',getArticles);
router.get('/:id',getArticle);
router.post('/',createArticle);
router.delete('/:id',deleteArticle);
router.put('/:id',updateArticle);



export default router;
const blogTitleField = document.querySelector('.title');
const articleField = document.querySelector('.article');

// banner
const publishBtn = document.querySelector('.publish-btn');

publishBtn.addEventListener('click',async (e)=>{
    e.preventDefault();
    if(articleField.value.length && blogTitleField.value.length){
        let date = new Date();// for published at info
        const res =await fetch('http://localhost:3000/articles',{
             method:"POST",
        headers: {
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title:blogTitleField.value,
            article:articleField.value,
             publishedAt:`${date.getDate()} ${date.getMonth()} ${date.getFullYear()}`
        })
           

        })    
    }
})

 async function getArticle() {
   // e.preventDefault();
    let url="http://localhost/articles"
    try{
        let res = await fetch(url);
        console.log(res);
        return res.json();
    }
    catch (e){
        console.log(e);
    }

}
getArticle();
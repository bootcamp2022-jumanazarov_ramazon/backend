import express from "express";
import articlesRoutes from './routes/articles.js'
import cors from "cors"

const PORT =3000;
const app = express();
app.use('/articles',articlesRoutes);
app.use(express.json());
app.use(cors());
app.use(express.static('public'));



app.listen(PORT,()=>{
    console.log(`Server is running on port : ${PORT}`)
})


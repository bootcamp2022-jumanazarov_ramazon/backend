# TASK 1 : 
## Write a command that searches files of a folder.
I use this command to searches files in my folder


```bash
directory$ ls|grep filename
```
 
 ## Example :
 There are many files and folders in my Darsliklar folder and I am going to find python book . For this , I should write this command :   Darsliklar$ ls|grep Python

![alt text](<./Images/task1.png>)

I get 2 results 

#


# TASK 2 : 
## Write a command that inserts the last 20 commands executed on your terminal into a file(ex: 2022-03-28_12-43-36.txt) and save it on your Desktop naming it with the current timestamp of the command executed.

In this task I used the following command :
```bash
history 20 > $(date "+%Y-%m-%d_%H-%M-%S")

```
I used " history 20 " for to give last 20 commands executed in my terminal and " $(date "+%Y-%m-%d_%H-%M-%S") " as name of my file.

## Example:

### 1)  There are no file in my Desktop
![alt text](<./Images/task2_1.png>)

### 2) After command executes , We can see a file named  2022-04-22_06-50-05

![alt text](<./Images/task2_2.png>)

### 3) And we open this file 


![alt text](<./Images/task2_3.png>)

#




 

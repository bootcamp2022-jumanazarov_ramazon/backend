package services

import (
	"fmt"

	"api_gateway/config"
	"api_gateway/genproto/position_service"
	"api_gateway/genproto/company_service"
	"google.golang.org/grpc"
)

type ServiceManager interface {
	ProfessionService() position_service.ProfessionServiceClient
	CompanyService()  company_service.CompanyServiceClient
	AttributeService() position_service.AttributeServiceClient
	PositionService()  position_service.PositionServiceClient
}

type grpcClients struct {
	professionService position_service.ProfessionServiceClient
	companyService company_service.CompanyServiceClient
	attributeService position_service.AttributeServiceClient
	positionService position_service.PositionServiceClient
}

func NewGrpcClients(conf *config.Config) (ServiceManager, error) {
	connPositionService, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.PositionServiceHost, conf.PositionServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	connCompanyService, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.CompanyServiceHost, conf.CompanyServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		professionService: position_service.NewProfessionServiceClient(connPositionService),
		companyService:company_service.NewCompanyServiceClient(connCompanyService),
		attributeService:position_service.NewAttributeServiceClient(connPositionService),
		positionService:position_service.NewPositionServiceClient(connPositionService),
	}, nil
}

func (g *grpcClients) ProfessionService() position_service.ProfessionServiceClient {
	return g.professionService
}
func (g *grpcClients) AttributeService() position_service.AttributeServiceClient {
	return g.attributeService
}

func (g *grpcClients) CompanyService() company_service.CompanyServiceClient {
	return g.companyService
}

func (g *grpcClients) PositionService() position_service.PositionServiceClient{
	return g.positionService
}
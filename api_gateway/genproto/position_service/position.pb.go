// Code generated by protoc-gen-go. DO NOT EDIT.
// source: position.proto

package position_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/protobuf/types/known/emptypb"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type PositionId struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PositionId) Reset()         { *m = PositionId{} }
func (m *PositionId) String() string { return proto.CompactTextString(m) }
func (*PositionId) ProtoMessage()    {}
func (*PositionId) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{0}
}

func (m *PositionId) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PositionId.Unmarshal(m, b)
}
func (m *PositionId) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PositionId.Marshal(b, m, deterministic)
}
func (m *PositionId) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PositionId.Merge(m, src)
}
func (m *PositionId) XXX_Size() int {
	return xxx_messageInfo_PositionId.Size(m)
}
func (m *PositionId) XXX_DiscardUnknown() {
	xxx_messageInfo_PositionId.DiscardUnknown(m)
}

var xxx_messageInfo_PositionId proto.InternalMessageInfo

func (m *PositionId) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type GetPositionAttribute struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value"`
	AttributeId          string   `protobuf:"bytes,3,opt,name=attribute_id,json=attributeId,proto3" json:"attribute_id"`
	PositionId           string   `protobuf:"bytes,4,opt,name=position_id,json=positionId,proto3" json:"position_id"`
	AttributeName        string   `protobuf:"bytes,5,opt,name=attribute_name,json=attributeName,proto3" json:"attribute_name"`
	AttributeType        string   `protobuf:"bytes,6,opt,name=attribute_type,json=attributeType,proto3" json:"attribute_type"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetPositionAttribute) Reset()         { *m = GetPositionAttribute{} }
func (m *GetPositionAttribute) String() string { return proto.CompactTextString(m) }
func (*GetPositionAttribute) ProtoMessage()    {}
func (*GetPositionAttribute) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{1}
}

func (m *GetPositionAttribute) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetPositionAttribute.Unmarshal(m, b)
}
func (m *GetPositionAttribute) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetPositionAttribute.Marshal(b, m, deterministic)
}
func (m *GetPositionAttribute) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetPositionAttribute.Merge(m, src)
}
func (m *GetPositionAttribute) XXX_Size() int {
	return xxx_messageInfo_GetPositionAttribute.Size(m)
}
func (m *GetPositionAttribute) XXX_DiscardUnknown() {
	xxx_messageInfo_GetPositionAttribute.DiscardUnknown(m)
}

var xxx_messageInfo_GetPositionAttribute proto.InternalMessageInfo

func (m *GetPositionAttribute) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *GetPositionAttribute) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

func (m *GetPositionAttribute) GetAttributeId() string {
	if m != nil {
		return m.AttributeId
	}
	return ""
}

func (m *GetPositionAttribute) GetPositionId() string {
	if m != nil {
		return m.PositionId
	}
	return ""
}

func (m *GetPositionAttribute) GetAttributeName() string {
	if m != nil {
		return m.AttributeName
	}
	return ""
}

func (m *GetPositionAttribute) GetAttributeType() string {
	if m != nil {
		return m.AttributeType
	}
	return ""
}

type Position struct {
	Id                   string                  `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Name                 string                  `protobuf:"bytes,2,opt,name=name,proto3" json:"name"`
	ProfessionId         string                  `protobuf:"bytes,3,opt,name=profession_id,json=professionId,proto3" json:"profession_id"`
	CompanyId            string                  `protobuf:"bytes,4,opt,name=company_id,json=companyId,proto3" json:"company_id"`
	PositionAttributes   []*GetPositionAttribute `protobuf:"bytes,5,rep,name=position_attributes,json=positionAttributes,proto3" json:"position_attributes"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *Position) Reset()         { *m = Position{} }
func (m *Position) String() string { return proto.CompactTextString(m) }
func (*Position) ProtoMessage()    {}
func (*Position) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{2}
}

func (m *Position) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Position.Unmarshal(m, b)
}
func (m *Position) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Position.Marshal(b, m, deterministic)
}
func (m *Position) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Position.Merge(m, src)
}
func (m *Position) XXX_Size() int {
	return xxx_messageInfo_Position.Size(m)
}
func (m *Position) XXX_DiscardUnknown() {
	xxx_messageInfo_Position.DiscardUnknown(m)
}

var xxx_messageInfo_Position proto.InternalMessageInfo

func (m *Position) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Position) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Position) GetProfessionId() string {
	if m != nil {
		return m.ProfessionId
	}
	return ""
}

func (m *Position) GetCompanyId() string {
	if m != nil {
		return m.CompanyId
	}
	return ""
}

func (m *Position) GetPositionAttributes() []*GetPositionAttribute {
	if m != nil {
		return m.PositionAttributes
	}
	return nil
}

type PositionAttributes struct {
	AttributeId          string   `protobuf:"bytes,1,opt,name=attribute_id,json=attributeId,proto3" json:"attribute_id"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PositionAttributes) Reset()         { *m = PositionAttributes{} }
func (m *PositionAttributes) String() string { return proto.CompactTextString(m) }
func (*PositionAttributes) ProtoMessage()    {}
func (*PositionAttributes) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{3}
}

func (m *PositionAttributes) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PositionAttributes.Unmarshal(m, b)
}
func (m *PositionAttributes) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PositionAttributes.Marshal(b, m, deterministic)
}
func (m *PositionAttributes) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PositionAttributes.Merge(m, src)
}
func (m *PositionAttributes) XXX_Size() int {
	return xxx_messageInfo_PositionAttributes.Size(m)
}
func (m *PositionAttributes) XXX_DiscardUnknown() {
	xxx_messageInfo_PositionAttributes.DiscardUnknown(m)
}

var xxx_messageInfo_PositionAttributes proto.InternalMessageInfo

func (m *PositionAttributes) GetAttributeId() string {
	if m != nil {
		return m.AttributeId
	}
	return ""
}

func (m *PositionAttributes) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type CreatePositionRequest struct {
	Name                 string                `protobuf:"bytes,1,opt,name=name,proto3" json:"name"`
	ProfessionId         string                `protobuf:"bytes,2,opt,name=profession_id,json=professionId,proto3" json:"profession_id"`
	CompanyId            string                `protobuf:"bytes,3,opt,name=company_id,json=companyId,proto3" json:"company_id"`
	PositionAttributes   []*PositionAttributes `protobuf:"bytes,4,rep,name=position_attributes,json=positionAttributes,proto3" json:"position_attributes"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *CreatePositionRequest) Reset()         { *m = CreatePositionRequest{} }
func (m *CreatePositionRequest) String() string { return proto.CompactTextString(m) }
func (*CreatePositionRequest) ProtoMessage()    {}
func (*CreatePositionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{4}
}

func (m *CreatePositionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreatePositionRequest.Unmarshal(m, b)
}
func (m *CreatePositionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreatePositionRequest.Marshal(b, m, deterministic)
}
func (m *CreatePositionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreatePositionRequest.Merge(m, src)
}
func (m *CreatePositionRequest) XXX_Size() int {
	return xxx_messageInfo_CreatePositionRequest.Size(m)
}
func (m *CreatePositionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreatePositionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreatePositionRequest proto.InternalMessageInfo

func (m *CreatePositionRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CreatePositionRequest) GetProfessionId() string {
	if m != nil {
		return m.ProfessionId
	}
	return ""
}

func (m *CreatePositionRequest) GetCompanyId() string {
	if m != nil {
		return m.CompanyId
	}
	return ""
}

func (m *CreatePositionRequest) GetPositionAttributes() []*PositionAttributes {
	if m != nil {
		return m.PositionAttributes
	}
	return nil
}

type UpdatePositionRequest struct {
	Id                   string                `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Name                 string                `protobuf:"bytes,2,opt,name=name,proto3" json:"name"`
	ProfessionId         string                `protobuf:"bytes,3,opt,name=profession_id,json=professionId,proto3" json:"profession_id"`
	CompanyId            string                `protobuf:"bytes,4,opt,name=company_id,json=companyId,proto3" json:"company_id"`
	PositionAttributes   []*PositionAttributes `protobuf:"bytes,5,rep,name=position_attributes,json=positionAttributes,proto3" json:"position_attributes"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *UpdatePositionRequest) Reset()         { *m = UpdatePositionRequest{} }
func (m *UpdatePositionRequest) String() string { return proto.CompactTextString(m) }
func (*UpdatePositionRequest) ProtoMessage()    {}
func (*UpdatePositionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{5}
}

func (m *UpdatePositionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdatePositionRequest.Unmarshal(m, b)
}
func (m *UpdatePositionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdatePositionRequest.Marshal(b, m, deterministic)
}
func (m *UpdatePositionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdatePositionRequest.Merge(m, src)
}
func (m *UpdatePositionRequest) XXX_Size() int {
	return xxx_messageInfo_UpdatePositionRequest.Size(m)
}
func (m *UpdatePositionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdatePositionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdatePositionRequest proto.InternalMessageInfo

func (m *UpdatePositionRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *UpdatePositionRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UpdatePositionRequest) GetProfessionId() string {
	if m != nil {
		return m.ProfessionId
	}
	return ""
}

func (m *UpdatePositionRequest) GetCompanyId() string {
	if m != nil {
		return m.CompanyId
	}
	return ""
}

func (m *UpdatePositionRequest) GetPositionAttributes() []*PositionAttributes {
	if m != nil {
		return m.PositionAttributes
	}
	return nil
}

type GetAllPositionRequest struct {
	Limit                uint32   `protobuf:"varint,1,opt,name=limit,proto3" json:"limit"`
	Offset               uint32   `protobuf:"varint,2,opt,name=offset,proto3" json:"offset"`
	Search               string   `protobuf:"bytes,3,opt,name=search,proto3" json:"search"`
	ProfessionId         string   `protobuf:"bytes,4,opt,name=profession_id,json=professionId,proto3" json:"profession_id"`
	CompanyId            string   `protobuf:"bytes,5,opt,name=company_id,json=companyId,proto3" json:"company_id"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetAllPositionRequest) Reset()         { *m = GetAllPositionRequest{} }
func (m *GetAllPositionRequest) String() string { return proto.CompactTextString(m) }
func (*GetAllPositionRequest) ProtoMessage()    {}
func (*GetAllPositionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{6}
}

func (m *GetAllPositionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllPositionRequest.Unmarshal(m, b)
}
func (m *GetAllPositionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllPositionRequest.Marshal(b, m, deterministic)
}
func (m *GetAllPositionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllPositionRequest.Merge(m, src)
}
func (m *GetAllPositionRequest) XXX_Size() int {
	return xxx_messageInfo_GetAllPositionRequest.Size(m)
}
func (m *GetAllPositionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllPositionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllPositionRequest proto.InternalMessageInfo

func (m *GetAllPositionRequest) GetLimit() uint32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

func (m *GetAllPositionRequest) GetOffset() uint32 {
	if m != nil {
		return m.Offset
	}
	return 0
}

func (m *GetAllPositionRequest) GetSearch() string {
	if m != nil {
		return m.Search
	}
	return ""
}

func (m *GetAllPositionRequest) GetProfessionId() string {
	if m != nil {
		return m.ProfessionId
	}
	return ""
}

func (m *GetAllPositionRequest) GetCompanyId() string {
	if m != nil {
		return m.CompanyId
	}
	return ""
}

type GetAllPositionResponse struct {
	Positions            []*Position `protobuf:"bytes,1,rep,name=positions,proto3" json:"positions"`
	Count                uint32      `protobuf:"varint,2,opt,name=count,proto3" json:"count"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

func (m *GetAllPositionResponse) Reset()         { *m = GetAllPositionResponse{} }
func (m *GetAllPositionResponse) String() string { return proto.CompactTextString(m) }
func (*GetAllPositionResponse) ProtoMessage()    {}
func (*GetAllPositionResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{7}
}

func (m *GetAllPositionResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllPositionResponse.Unmarshal(m, b)
}
func (m *GetAllPositionResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllPositionResponse.Marshal(b, m, deterministic)
}
func (m *GetAllPositionResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllPositionResponse.Merge(m, src)
}
func (m *GetAllPositionResponse) XXX_Size() int {
	return xxx_messageInfo_GetAllPositionResponse.Size(m)
}
func (m *GetAllPositionResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllPositionResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllPositionResponse proto.InternalMessageInfo

func (m *GetAllPositionResponse) GetPositions() []*Position {
	if m != nil {
		return m.Positions
	}
	return nil
}

func (m *GetAllPositionResponse) GetCount() uint32 {
	if m != nil {
		return m.Count
	}
	return 0
}

type CreatePosAttributeRequest struct {
	Value                string   `protobuf:"bytes,1,opt,name=value,proto3" json:"value"`
	AttributeId          string   `protobuf:"bytes,2,opt,name=attribute_id,json=attributeId,proto3" json:"attribute_id"`
	PositionId           string   `protobuf:"bytes,3,opt,name=position_id,json=positionId,proto3" json:"position_id"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreatePosAttributeRequest) Reset()         { *m = CreatePosAttributeRequest{} }
func (m *CreatePosAttributeRequest) String() string { return proto.CompactTextString(m) }
func (*CreatePosAttributeRequest) ProtoMessage()    {}
func (*CreatePosAttributeRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_56e266f1a28a7893, []int{8}
}

func (m *CreatePosAttributeRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreatePosAttributeRequest.Unmarshal(m, b)
}
func (m *CreatePosAttributeRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreatePosAttributeRequest.Marshal(b, m, deterministic)
}
func (m *CreatePosAttributeRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreatePosAttributeRequest.Merge(m, src)
}
func (m *CreatePosAttributeRequest) XXX_Size() int {
	return xxx_messageInfo_CreatePosAttributeRequest.Size(m)
}
func (m *CreatePosAttributeRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreatePosAttributeRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreatePosAttributeRequest proto.InternalMessageInfo

func (m *CreatePosAttributeRequest) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

func (m *CreatePosAttributeRequest) GetAttributeId() string {
	if m != nil {
		return m.AttributeId
	}
	return ""
}

func (m *CreatePosAttributeRequest) GetPositionId() string {
	if m != nil {
		return m.PositionId
	}
	return ""
}

func init() {
	proto.RegisterType((*PositionId)(nil), "position_service.PositionId")
	proto.RegisterType((*GetPositionAttribute)(nil), "position_service.GetPositionAttribute")
	proto.RegisterType((*Position)(nil), "position_service.Position")
	proto.RegisterType((*PositionAttributes)(nil), "position_service.PositionAttributes")
	proto.RegisterType((*CreatePositionRequest)(nil), "position_service.CreatePositionRequest")
	proto.RegisterType((*UpdatePositionRequest)(nil), "position_service.UpdatePositionRequest")
	proto.RegisterType((*GetAllPositionRequest)(nil), "position_service.GetAllPositionRequest")
	proto.RegisterType((*GetAllPositionResponse)(nil), "position_service.GetAllPositionResponse")
	proto.RegisterType((*CreatePosAttributeRequest)(nil), "position_service.CreatePosAttributeRequest")
}

func init() { proto.RegisterFile("position.proto", fileDescriptor_56e266f1a28a7893) }

var fileDescriptor_56e266f1a28a7893 = []byte{
	// 583 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xbc, 0x95, 0xcb, 0x6e, 0xd3, 0x4c,
	0x14, 0xc7, 0x35, 0xb9, 0xe9, 0xcb, 0x49, 0xe3, 0x0f, 0x0d, 0x49, 0xe4, 0xa6, 0x45, 0x14, 0x73,
	0xcb, 0xca, 0x91, 0xca, 0x86, 0x6d, 0xb9, 0x45, 0x59, 0x80, 0x90, 0x21, 0xaa, 0xc4, 0xa6, 0x72,
	0xe2, 0x93, 0xd4, 0x92, 0xe3, 0x19, 0x3c, 0xe3, 0x4a, 0x79, 0x1c, 0x1e, 0x83, 0x17, 0x60, 0x83,
	0xc4, 0x92, 0xe7, 0x41, 0x19, 0xdf, 0x1a, 0xdb, 0xb5, 0x25, 0x16, 0xec, 0x72, 0x8e, 0xcf, 0x4c,
	0xfe, 0xbf, 0xff, 0x99, 0x33, 0x03, 0x1a, 0x67, 0xc2, 0x95, 0x2e, 0xf3, 0x4d, 0x1e, 0x30, 0xc9,
	0xe8, 0xbd, 0x24, 0xbe, 0x12, 0x18, 0xdc, 0xb8, 0x2b, 0x1c, 0x9f, 0x6c, 0x18, 0xdb, 0x78, 0x38,
	0x55, 0xdf, 0x97, 0xe1, 0x7a, 0x8a, 0x5b, 0x2e, 0x77, 0x51, 0xb9, 0x71, 0x0a, 0xf0, 0x31, 0x5e,
	0x30, 0x77, 0xa8, 0x06, 0x0d, 0xd7, 0xd1, 0xc9, 0x19, 0x99, 0x74, 0xad, 0x86, 0xeb, 0x18, 0xbf,
	0x08, 0x0c, 0x66, 0x28, 0x93, 0x8a, 0x0b, 0x29, 0x03, 0x77, 0x19, 0x4a, 0xcc, 0x17, 0xd2, 0x01,
	0xb4, 0x6f, 0x6c, 0x2f, 0x44, 0xbd, 0xa1, 0x52, 0x51, 0x40, 0x1f, 0xc1, 0x91, 0x9d, 0x2c, 0xb9,
	0x72, 0x1d, 0xbd, 0xa9, 0x3e, 0xf6, 0xd2, 0xdc, 0xdc, 0xa1, 0x0f, 0xa1, 0x97, 0x0a, 0x76, 0x1d,
	0xbd, 0xa5, 0x2a, 0x80, 0x67, 0x92, 0x9e, 0x82, 0x96, 0xed, 0xe1, 0xdb, 0x5b, 0xd4, 0xdb, 0xaa,
	0xa6, 0x9f, 0x66, 0x3f, 0xd8, 0x5b, 0x3c, 0x2c, 0x93, 0x3b, 0x8e, 0x7a, 0x27, 0x57, 0xf6, 0x79,
	0xc7, 0xd1, 0xf8, 0x49, 0xe0, 0xbf, 0x84, 0xa6, 0x00, 0x41, 0xa1, 0xa5, 0xfe, 0x20, 0x62, 0x50,
	0xbf, 0xe9, 0x63, 0xe8, 0xf3, 0x80, 0xad, 0x51, 0x88, 0x58, 0x61, 0xc4, 0x70, 0x94, 0x25, 0xe7,
	0x0e, 0x7d, 0x00, 0xb0, 0x62, 0x5b, 0x6e, 0xfb, 0xbb, 0x8c, 0xa1, 0x1b, 0x67, 0xe6, 0x0e, 0xbd,
	0x84, 0xfb, 0x29, 0x63, 0x2a, 0x47, 0xe8, 0xed, 0xb3, 0xe6, 0xa4, 0x77, 0xfe, 0xcc, 0xcc, 0x37,
	0xcc, 0x2c, 0x73, 0xdc, 0xa2, 0x3c, 0x9f, 0x12, 0xc6, 0x7b, 0xa0, 0x85, 0x42, 0x51, 0x70, 0x9d,
	0x14, 0x5d, 0x2f, 0x6d, 0x97, 0xf1, 0x83, 0xc0, 0xf0, 0x75, 0x80, 0xb6, 0xc4, 0x64, 0x57, 0x0b,
	0xbf, 0x86, 0x28, 0x64, 0xea, 0x0c, 0xa9, 0x72, 0xa6, 0x51, 0xeb, 0x4c, 0x33, 0xef, 0xcc, 0xa2,
	0xdc, 0x99, 0x96, 0x72, 0xe6, 0x49, 0xd1, 0x99, 0x22, 0x6d, 0xa9, 0x2f, 0xbf, 0x09, 0x0c, 0x17,
	0xdc, 0x29, 0x01, 0xf9, 0x57, 0x2d, 0x5f, 0x54, 0xb5, 0xfc, 0xef, 0xc1, 0xbe, 0x11, 0x18, 0xce,
	0x50, 0x5e, 0x78, 0x5e, 0x1e, 0x6c, 0x00, 0x6d, 0xcf, 0xdd, 0xba, 0x52, 0xb1, 0xf5, 0xad, 0x28,
	0xa0, 0x23, 0xe8, 0xb0, 0xf5, 0x5a, 0xa0, 0x54, 0x80, 0x7d, 0x2b, 0x8e, 0xf6, 0x79, 0x81, 0x76,
	0xb0, 0xba, 0x8e, 0xd9, 0xe2, 0xa8, 0x88, 0xde, 0xaa, 0x45, 0x6f, 0xe7, 0xd0, 0x8d, 0x6b, 0x18,
	0xe5, 0x25, 0x0a, 0xce, 0x7c, 0x81, 0xf4, 0x25, 0x74, 0x13, 0x26, 0xa1, 0x13, 0x65, 0xc5, 0xf8,
	0x6e, 0x2b, 0xac, 0xac, 0x78, 0x4f, 0xb7, 0x62, 0xa1, 0x9f, 0x60, 0x44, 0x81, 0x11, 0xc2, 0x71,
	0x7a, 0x5c, 0xb3, 0x41, 0xc9, 0x0c, 0x89, 0x8e, 0x38, 0xa9, 0xba, 0x91, 0x1a, 0xb5, 0x37, 0x52,
	0x33, 0x7f, 0x23, 0x9d, 0x7f, 0x6f, 0xc2, 0xff, 0x89, 0xc8, 0x4f, 0x91, 0x68, 0x7a, 0x09, 0xda,
	0xe1, 0xe4, 0xd0, 0xe7, 0x45, 0xb2, 0xd2, 0xd9, 0x1a, 0x9f, 0xde, 0x6d, 0xc1, 0xdc, 0xa1, 0x2b,
	0xd0, 0x0e, 0xdd, 0x2c, 0xdb, 0xb8, 0xf4, 0x48, 0x8c, 0x27, 0xf5, 0x85, 0x71, 0x63, 0x66, 0xd0,
	0xbb, 0x75, 0xe7, 0xd0, 0x4a, 0x45, 0xe3, 0x8a, 0x96, 0xed, 0x6d, 0x38, 0x9c, 0xbb, 0x32, 0xb5,
	0xa5, 0x93, 0x59, 0x63, 0xc3, 0x3b, 0xd0, 0xde, 0xa0, 0x87, 0xb7, 0x36, 0xae, 0x16, 0x39, 0x32,
	0xa3, 0x47, 0xcf, 0x4c, 0x1e, 0x3d, 0xf3, 0xed, 0xfe, 0xd1, 0x7b, 0x75, 0xf2, 0xe5, 0x78, 0x83,
	0xbe, 0x4a, 0x4e, 0xf3, 0xeb, 0x97, 0x1d, 0x95, 0x7f, 0xf1, 0x27, 0x00, 0x00, 0xff, 0xff, 0x4b,
	0x9a, 0x02, 0x43, 0x53, 0x07, 0x00, 0x00,
}

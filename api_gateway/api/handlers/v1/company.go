package v1

import (
	"api_gateway/genproto/company_service"
	"net/http"
	"github.com/gin-gonic/gin"
)

// CreateCompany godoc
// @ID create-company
// @Router /v1/company [POST]
// @Summary create company
// @Description create company
// @Tags company
// @Accept json
// @Produce json
// @Param company body company_service.CreateCompanyRequest true "company"
// @Success 200 {object} models.ResponseModel{data=company_service.Company} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateCompany(c *gin.Context) {
	var company company_service.CreateCompanyRequest

	if err := c.BindJSON(&company); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

	resp, err := h.services.CompanyService().Create(c.Request.Context(), &company)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while creating company", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusCreated, "ok", resp)
}

// GetAllCompany godoc
// @ID get-all-company
// @Router /v1/company [get]
// @Summary get all company
// @Description get company
// @Tags company
// @Accept json
// @Produce json
// @Param search query string false "search"
// @Param limit query integer false "limit"
// @Param offset query integer false "offset"
// @Success 200 {object} models.ResponseModel{data=company_service.CompanyGettAllResponse} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllCompany(c *gin.Context) {
	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	resp, err := h.services.CompanyService().GetAll(
		c.Request.Context(),
		&company_service.CompanyGettAllRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error getting all companies", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "OK", resp)
}

// GetCompanyByID godoc
// @ID get-company-by-id
// @Router /v1/company/:id [GET]
// @Summary get company by id
// @Description get company by id
// @Tags company
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{data=company_service.Company} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetCompanyByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.CompanyService().Get(c.Request.Context(),
		&company_service.CompanyId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting company", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// UpdateCompany godoc
// @ID update-company-by-id
// @Router /v1/company/:id [PUT]
// @Summary update company by id
// @Description update company by id
// @Tags company
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Param company body company_service.CreateCompanyRequest true "company"
// @Success 200 {object} models.ResponseModel{data=company_service.CompanyId} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateCompanyByID(c *gin.Context) {

	id := c.Query("id")
	var company company_service.CreateCompanyRequest
	if err := c.BindJSON(&company); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.CompanyService().Update(c.Request.Context(),
		&company_service.Company{
			Id:   id,
			Name: company.Name,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while updating company", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}


// DeleteCompany godoc
// @ID delete-company-by-id
// @Router /v1/company/:id [DELETE]
// @Summary delete company by id
// @Description delete company by id
// @Tags company
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteCompanyByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.CompanyService().Delete(c.Request.Context(),
		&company_service.CompanyId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while deleting company", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}
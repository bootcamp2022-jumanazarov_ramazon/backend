package v1

import (
	"api_gateway/genproto/position_service"
	"api_gateway/genproto/company_service"
	"net/http"
	"github.com/gin-gonic/gin"
)

// CreatePosition godoc
// @ID create-position
// @Router /v1/position [POST]
// @Summary create position
// @Description create position
// @Tags position
// @Accept json
// @Produce json
// @Param position body position_service.CreatePositionRequest true "position"
// @Success 200 {object} models.ResponseModel{data=position_service.Position} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreatePosition(c *gin.Context) {
	var position position_service.CreatePositionRequest

	if err := c.BindJSON(&position); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
    _,err := h.services.CompanyService().Get(c.Request.Context(),
		&company_service.CompanyId{
			Id: position.CompanyId,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting company", err)
		return
	}	
	resp, err := h.services.PositionService().CreatePosition(c.Request.Context(), &position)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while creating position", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusCreated, "ok", resp)
}

// GetAllPosition godoc
// @ID get-all-position
// @Router /v1/position [get]
// @Summary get all position
// @Description get position
// @Tags position
// @Accept json
// @Produce json
// @Param search query string false "search"
// @Param limit query integer false "limit"
// @Param offset query integer false "offset"
// @Param profession_id query string false "profession_id"
// @Param company_id query string false "company_id"
// @Success 200 {object} models.ResponseModel{data=position_service.GetAllPositionResponse} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllPosition(c *gin.Context) {
	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}
	// professionId, check := c.GetQuery("profession_id")
	// if !check  {
	// 	return
	// }
	companyId:=c.Query("company_id")
	// if !check  {
	// 	return
	// }
	if companyId != ""{
    _,err = h.services.CompanyService().Get(c.Request.Context(),
		&company_service.CompanyId{
			Id: string(companyId),
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting company", err)
		return
	}	
	}

	resp, err := h.services.PositionService().GetAllPosition(
		c.Request.Context(),
		&position_service.GetAllPositionRequest{
			Limit:  uint32(limit),
			Offset: uint32(offset),
			Search: c.Query("search"),
			ProfessionId: c.Query("professionId"),
			CompanyId: companyId,

		},
	)

	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error getting all positions", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "OK", resp)
}

// GetPositionByID godoc
// @ID get-position-by-id
// @Router /v1/position/:id [GET]
// @Summary get position by id
// @Description get position by id
// @Tags position
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{data=position_service.Position} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetPositionByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.PositionService().GetPosition(c.Request.Context(),
		&position_service.PositionId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting position", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// UpdatePosition godoc
// @ID update-position-by-id
// @Router /v1/position/:id [PUT]
// @Summary update position by id
// @Description update position by id
// @Tags position
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Param position body position_service.CreatePositionRequest true "position"
// @Success 200 {object} models.ResponseModel{data=position_service.Position} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdatePositionByID(c *gin.Context) {
    
	id := c.Query("id")
	var position position_service.CreatePositionRequest
	if err := c.BindJSON(&position); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

    _,err := h.services.CompanyService().Get(c.Request.Context(),
		&company_service.CompanyId{
			Id: position.CompanyId,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting company", err)
		return
	}	
	resp, err := h.services.PositionService().UpdatePosition(c.Request.Context(),
		&position_service.UpdatePositionRequest{
			Id:   id,
			Name: position.Name,
			ProfessionId: position.ProfessionId,
			CompanyId: position.CompanyId,
			PositionAttributes: position.PositionAttributes,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting position id", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}


// DeletePosition godoc
// @ID delete-position-by-id
// @Router /v1/position/:id [DELETE]
// @Summary delete position by id
// @Description delete position by id
// @Tags position
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeletePositionByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.PositionService().DeletePosition(c.Request.Context(),
		&position_service.PositionId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while delete position", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}
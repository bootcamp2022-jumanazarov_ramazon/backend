package v1

import (
	"api_gateway/genproto/position_service"
	"net/http"
	"github.com/gin-gonic/gin"
)

// CreateAttribute godoc
// @ID create-attribute
// @Router /v1/attribute [POST]
// @Summary create attribute
// @Description create attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute body position_service.CreateAttributeRequest true "attribute"
// @Success 200 {object} models.ResponseModel{data=position_service.Attribute} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateAttribute(c *gin.Context) {
	var attribute position_service.CreateAttributeRequest

	if err := c.BindJSON(&attribute); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

	resp, err := h.services.AttributeService().Create(c.Request.Context(), &attribute)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while creating attribute", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusCreated, "ok", resp)
}

// GetAllAttribute godoc
// @ID get-all-attribute
// @Router /v1/attribute [get]
// @Summary get all attribute
// @Description get attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param search query string false "search"
// @Param limit query integer false "limit"
// @Param offset query integer false "offset"
// @Success 200 {object} models.ResponseModel{data=position_service.GetAllAttributeResponse} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllAttribute(c *gin.Context) {
	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	resp, err := h.services.AttributeService().GetAll(
		c.Request.Context(),
		&position_service.GetAllAttributeRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error getting all attributes", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "OK", resp)
}

// GetAttributeByID godoc
// @ID get-attribute-by-id
// @Router /v1/attribute/:id [GET]
// @Summary get attribute by id
// @Description get attribute by id
// @Tags attribute
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{data=position_service.Attribute} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAttributeByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.AttributeService().Get(c.Request.Context(),
		&position_service.AttributeId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while getting attribute", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// UpdateAttribute godoc
// @ID update-attribute-by-id
// @Router /v1/attribute/:id [PUT]
// @Summary update attribute by id
// @Description update attribute by id
// @Tags attribute
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Param attribute body position_service.CreateAttributeRequest true "attribute"
// @Success 200 {object} models.ResponseModel{data=position_service.Attribute} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateAttributeByID(c *gin.Context) {

	id := c.Query("id")
	var attribute position_service.CreateAttributeRequest
	if err := c.BindJSON(&attribute); err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.AttributeService().Update(c.Request.Context(),
		&position_service.Attribute{
			Id:   id,
			Name: attribute.Name,
			AttributeType: attribute.AttributeType,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while updating attribute", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}


// DeleteAttribute godoc
// @ID delete-attribute-by-id
// @Router /v1/attribute/:id [DELETE]
// @Summary delete attribute by id
// @Description delete attribute by id
// @Tags attribute
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Success 200 {object} models.ResponseModel{} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteAttributeByID(c *gin.Context) {

	id := c.Query("id")
	resp, err := h.services.AttributeService().Delete(c.Request.Context(),
		&position_service.AttributeId{
			Id: id,
		})
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while delete attribute", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}
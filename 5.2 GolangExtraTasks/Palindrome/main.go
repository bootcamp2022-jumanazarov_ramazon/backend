package main

import (
	"fmt"
	"strings"
)

func palindrome(text string) bool {
	n := len(text)
	text = strings.ToLower(text)
	for i := 0; i <= n/2; i++ {
		if text[i] != text[n-1-i] {
			return false
		}
	}
	return true
}

func main() {
	words := []string{"level", "Madam", "book", "golang"}
	for _, word := range words {
		if palindrome(word) {
			fmt.Printf("%s is a palindrome word \n", word)
		} else {
			fmt.Printf("%s is not a palindrome word \n", word)
		}
	}
}

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	var n int
	fmt.Scan(&n)
	var a = make([][]int, n)
	for i := range a {
		a[i] = make([]int, n)
	}
	var b = make([][]int, n)
	for i := range b {
		b[i] = make([]int, n)
	}

	fmt.Println("First Array")

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			a[i][j] = rand.Intn(10)
			fmt.Printf("%d ", a[i][j])
		}
		fmt.Println("")
	}
	fmt.Println("Second Array")
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			b[i][j] = rand.Intn(10)
			fmt.Printf("%d ", b[i][j])
		}
		fmt.Println("")
	}
	fmt.Println("Result")
	c := MultiplyMassive(a, b, n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			fmt.Printf("%d ", c[i][j])
		}
		fmt.Println("")
	}

}

func MultiplyMassive(a, b [][]int, n int) [][]int {
	var result = make([][]int, n)
	for i := range a {
		result[i] = make([]int, n)
	}
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			for k := 0; k < n; k++ {
				result[i][j] += a[i][k] * b[k][j]
			}
		}

	}
	return result
}

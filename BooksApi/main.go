package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"BooksApi/handlers"
    )
func main(){

  r:=gin.Default()
  userRoutes:=r.Group("/books")
  {
	  	userRoutes.GET("/", handlers.GetBooksList)
        userRoutes.POST("/",handlers.CreateBook)
		userRoutes.PUT("/:id",handlers.EditBook)
		userRoutes.DELETE("/:id",handlers.DeleteBook)

  }
  

  if err := r.Run(":5000");err!=nil{
    log.Fatal(err)}

}


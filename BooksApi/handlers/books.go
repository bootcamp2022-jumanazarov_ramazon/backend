package handlers

import (
	"BooksApi/models"
	"github.com/gin-gonic/gin"
	"fmt"
)

var Books []models.Book

func GetBooksList(c *gin.Context){
	c.JSON(200,Books)
}

func CreateBook(c *gin.Context){
	
	var reqBody models.Book
	if err:=c.ShouldBindJSON(&reqBody);err!=nil{
		fmt.Println(reqBody)
		c.JSON(422,gin.H{
			"error":true,
			"message":"Invalid request body",
		})
		return	
	}

	Books = append(Books,reqBody)
	c.JSON(200,gin.H{
		"error":false,
	})
}

func EditBook(c *gin.Context){
	id := c.Param("id")
	var reqBody models.Book
	if err:=c.ShouldBindJSON(&reqBody);err!=nil{
       c.JSON(422,gin.H{
		   "error":true,
		   "message":"Invalid request body",
	   })
	   return
	}
	for i,book := range Books{
		if string(book.ID)==id {
			Books[i].Name=reqBody.Name
			Books[i].PublishYear=reqBody.PublishYear
			Books[i].Author.FirstName=reqBody.Author.FirstName
			Books[i].Author.LastName=reqBody.Author.LastName
			c.JSON(200,gin.H{
				"error":"false",
			})
			return
			}
			
	}
	c.JSON(404,gin.H{
		"error":true,
		"message":"Invalid User Id",
	})
}

func DeleteBook(c *gin.Context){
	id:=c.Param("id")
	for i,book := range Books{
		if string(book.ID)==id {
			Books=append(Books[:i],Books[i+1:]...)
			c.JSON(200,gin.H{
				"error":"false",
			})
			return
			}	
	}
	c.JSON(404,gin.H{
		"error":true,
		"message":"Invalid Book Id",
	})
}

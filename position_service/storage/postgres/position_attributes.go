package postgres

import (
	"context"
	"fmt"
	// "position_service/pkg/helper"
	// "position_service/storage"

	pb "position_service/genproto/position_service"

	"github.com/google/uuid"
	// "github.com/jackc/pgx/v4"

	// "github.com/jackc/pgx/v4/pgxpool"
)



func (r *positionRepo) CreatePosAttribute(ctx context.Context, req *pb.CreatePosAttributeRequest) (id string,err error) {

	newid := uuid.New().String()
	query := `INSERT INTO position_attributes(id,attribute_id,position_id,value)
	VALUES ($1,$2,$3,$4) `

	raw, err := r.db.Query(ctx, query, newid, req.AttributeId, req.PositionId, req.Value)
	if err != nil {
		return "",err
	}
     raw.Scan(&id,)
	
	return newid,err

}

func (r *positionRepo) GetPosAttributes(ctx context.Context,req *pb.PositionId)(resp []*pb.GetPositionAttribute,err error){


	queryPosAtt := `SELECT
  pa.id,
  pa.value,
  pa.attribute_id,
  pa.position_id,
  a.name,
  a.type
  from position_attributes as pa
  JOIN attribute as a ON pa.attribute_id = a.id 
  WHERE pa.position_id = $1
  `
	rows, err := r.db.Query(ctx, queryPosAtt, req.Id)
	if err != nil {
		return nil, fmt.Errorf("error while getting position attributes")
	}

	for rows.Next() {
		var pos_attribute pb.GetPositionAttribute

		err = rows.Scan(
			&pos_attribute.Id,
			&pos_attribute.Value,
			&pos_attribute.AttributeId,
			&pos_attribute.PositionId,
			&pos_attribute.AttributeName,
			&pos_attribute.AttributeType,
		)

		if err != nil {
			return nil, fmt.Errorf("error while scanning position attribute err: %w", err)
		}
        fmt.Println(pos_attribute)
		resp = append(resp, &pos_attribute)
	}
   return resp, err
}

func (r *positionRepo) UpdatePosAttribute(ctx context.Context,req *pb.CreatePosAttributeRequest)(string,error){

    var Id string
	query := `UPDATE position_attributes 
	SET
	value = $1 
	WHERE 
	position_id = $2 AND attribute_id = $3
	returning id
	`
	raw,err := r.db.Exec(ctx,query,req.Value,req.PositionId,req.AttributeId)
	if err != nil  {
		return "",fmt.Errorf("error while updating position attributes %w",err)
	}
	if raw.RowsAffected()!=1 {
		Id,err = r.CreatePosAttribute(ctx,&pb.CreatePosAttributeRequest{
			Value: req.Value,
			AttributeId: req.AttributeId,
			PositionId: req.PositionId,
		})
		if err != nil  {
		return "",fmt.Errorf("error while updating position attributes %w",err)
	}
	return Id,nil
}

    getQuey:=`SELECT id FROM position_attributes WHERE attribute_id = $1 AND position_id = $2`
	row,err := r.db.Query(ctx,getQuey,req.AttributeId,req.PositionId)
	if err != nil  {
		return "",fmt.Errorf("error while getting position attribute id %w",err)
	}
	for row.Next(){
	err = row.Scan(&Id)
	if err != nil  {
		return "",fmt.Errorf("error while scanning position attribute id %w",err)
	}
    }
	return Id,nil
}
package postgres

import (
	"context"
	"fmt"
	"position_service/pkg/helper"
	"position_service/storage"

	pb "position_service/genproto/position_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)
type attributeRepo struct {
	db *pgxpool.Pool
}

func NewAttributeRepo(db *pgxpool.Pool) storage.AttributeI {
	return &attributeRepo{
		db: db,
	}
}

func (r *attributeRepo) Create(ctx context.Context, req *pb.CreateAttributeRequest)(id string , err error){

	query := `
		INSERT INTO attribute (
			id,
			name,
			type
		) 
		 VALUES ($1, $2, $3)
		 returning id
	`

	id = uuid.New().String()
	
   
	raw, err := r.db.Exec(
		ctx,
		query,
		id,
		req.Name,
		req.AttributeType,
	)

	if raw.RowsAffected() != 1{
		return "",fmt.Errorf("error while creating attribute")
	}

	if err != nil {
		return "", fmt.Errorf("error while inserting attribute err: %w", err)
	}

	return id, nil
}

func (r *attributeRepo) GetAll(ctx context.Context, req *pb.GetAllAttributRequest) (*pb.GetAllAttributResponse, error) {
	var (
		resp   pb.GetAllAttributResponse
		err    error
		filter string
		params = make(map[string]interface{})
	)

	if req.Search != "" {
		filter += " AND name ILIKE '%' || :search || '%' "
		params["search"] = req.Search
	}

	countQuery := `SELECT count(1) FROM attribute WHERE true ` + filter

	q, arr := helper.ReplaceQueryParams(countQuery, params)
	err = r.db.QueryRow(ctx, q, arr...).Scan(
		&resp.Count,
	)

	if err != nil {
		return nil, fmt.Errorf("error while scanning count %w", err)
	}

	query := `SELECT
				id,
				name,
				type
			FROM attribute
			WHERE true` + filter

	query += " LIMIT :limit OFFSET :offset"
	params["limit"] = req.Limit
	params["offset"] = req.Offset

	q, arr = helper.ReplaceQueryParams(query, params)
	rows, err := r.db.Query(ctx, q, arr...)

	if err != nil {
		return nil, fmt.Errorf("error while getting rows %w", err)
	}
	for rows.Next() {
		var attribute pb.Attribute

		err = rows.Scan(
			&attribute.Id,
			&attribute.Name,
			&attribute.AttributeType,
		)

		if err != nil {
			return nil, fmt.Errorf("error while scanning attribute err: %w", err)
		}

		resp.Attributes = append(resp.Attributes,&attribute)
	}
	return &resp, nil
 }

func (r *attributeRepo) Get(ctx context.Context,req *pb.AttributeId) (*pb.Attribute,error){
    query:=`SELECT 
	id,
	name,
	type
	from attribute
	WHERE id=$1`
   
	var val pb.Attribute
   err := r.db.QueryRow(ctx,query,req.Id).Scan(&val.Id,&val.Name,&val.AttributeType)

   if err != nil {
	return nil, fmt.Errorf("error while scanning err: %w", err)
   }
	return &val,nil
}


func (r *attributeRepo) Delete(ctx context.Context,entity *pb.AttributeId) (error){
    query:=`DELETE
	from attribute
	WHERE id=$1`
	id,err:=uuid.Parse(entity.Id)
	
	if err != nil {

	return  fmt.Errorf("error while getting id err: %w", err)
   }
   row,err := r.db.Exec(ctx,query,id)
   if err != nil {

	return  fmt.Errorf("error while scanning err: %w", err)
   }
   if row.RowsAffected()!=1{
	return fmt.Errorf("attribute not found ")
   }
	return nil
}


func (r *attributeRepo) Update(ctx context.Context,req *pb.Attribute) (error){
    query:=`UPDATE 
	 attribute 
	 SET 
	name = $1 ,
	type = $2
	WHERE id=$3`
   
	raw, err := r.db.Exec(
		ctx,
		query,
		req.Name,
		req.AttributeType,
		req.Id,
	)
   if err != nil {
	return fmt.Errorf("error while scanning err: %w", err)
   }
   if raw.RowsAffected()!=1{
	return fmt.Errorf("attribute not found")
   }
	return nil
}

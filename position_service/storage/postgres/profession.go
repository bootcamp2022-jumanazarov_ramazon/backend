package postgres

import (
	"context"
	"fmt"
	"position_service/pkg/helper"
	"position_service/storage"

	pb "position_service/genproto/position_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type professionRepo struct {
	db *pgxpool.Pool
}

func NewProfessionRepo(db *pgxpool.Pool) storage.ProfessionI {
	return &professionRepo{
		db: db,
	}
}

func (r *professionRepo) Create(ctx context.Context, entity *pb.CreateProfessionRequest) (id string, err error) {
	query := `
		INSERT INTO profession (
			id,
			name
		) 
		 VALUES ($1, $2)
	`

	id = uuid.New().String()
	

	_, err = r.db.Exec(
		ctx,
		query,
		id,
		entity.Name,
	)

	if err != nil {
		return "", fmt.Errorf("error while inserting profession err: %w", err)
	}

	return id, nil
}

func (r *professionRepo) GetAll(ctx context.Context, req *pb.GetAllProfessionRequest) (*pb.GetAllProfessionResponse, error) {
	var (
		resp   pb.GetAllProfessionResponse
		err    error
		filter string
		params = make(map[string]interface{})
	)

	if req.Search != "" {
		filter += " AND name ILIKE '%' || :search || '%' "
		params["search"] = req.Search
	}

	countQuery := `SELECT count(1) FROM profession WHERE true ` + filter

	q, arr := helper.ReplaceQueryParams(countQuery, params)
	err = r.db.QueryRow(ctx, q, arr...).Scan(
		&resp.Count,
	)

	if err != nil {
		return nil, fmt.Errorf("error while scanning count %w", err)
	}

	query := `SELECT
				id,
				name
			FROM profession
			WHERE true` + filter

	query += " LIMIT :limit OFFSET :offset"
	params["limit"] = req.Limit
	params["offset"] = req.Offset

	q, arr = helper.ReplaceQueryParams(query, params)
	rows, err := r.db.Query(ctx, q, arr...)

	if err != nil {
		return nil, fmt.Errorf("error while getting rows %w", err)
	}
	for rows.Next() {
		var profession pb.Profession

		err = rows.Scan(
			&profession.Id,
			&profession.Name,
		)

		if err != nil {
			return nil, fmt.Errorf("error while scanning profession err: %w", err)
		}

		resp.Professions = append(resp.Professions,&profession)
	}
	return &resp, nil
 }

func (r *professionRepo) GetByID(ctx context.Context,req *pb.GetByIDProfessionRequest) (*pb.Profession,error){
    query:=`SELECT 
	id,
	name
	from profession
	WHERE id=$1`
   
	var val pb.Profession
   err := r.db.QueryRow(ctx,query,req.Id).Scan(&val.Id,&val.Name)

   if err != nil {
	return nil, fmt.Errorf("error while scanning err: %w", err)
   }
	return &val,nil
}

func (r *professionRepo) DeleteByID(ctx context.Context,entity *pb.GetByIDProfessionRequest) (error){
    query:=`DELETE
	from profession
	WHERE id=$1`
	id,err:=uuid.Parse(entity.Id)
	
	if err != nil {

	return  fmt.Errorf("error while scanning id err: %w", err)
   }
   row,err := r.db.Exec(ctx,query,id)
   if err != nil {

	return  fmt.Errorf("error while scanning err: %w", err)
   }
   if row.RowsAffected()!=1{
	return fmt.Errorf("profession not found ")
   }
	return nil
}


func (r *professionRepo) UpdateByID(ctx context.Context,req *pb.Profession) (error){
    query:=`UPDATE 
	 profession 
	 SET 
	name = $1
	WHERE id=$2`
   
	raw, err := r.db.Exec(
		ctx,
		query,
		req.Name,
		req.Id,
	)
   if err != nil {
	return fmt.Errorf("error while scanning err: %w", err)
   }
   if raw.RowsAffected()!=1{
	return fmt.Errorf("profession not found")
   }
	return nil
}

CREATE TYPE  "attribute_types" AS ENUM (
  'datetime',
  'text',
  'number'
);

CREATE TABLE IF NOT EXISTS "profession" (
  "id" uuid PRIMARY KEY,
  "name" varchar(255)
);

CREATE TABLE IF NOT EXISTS "position" (
  "id" uuid PRIMARY KEY,
  "name" varchar(255),
  "profession_id" uuid,
  "company_id" uuid
  
);

CREATE TABLE IF NOT EXISTS "attribute" (
  "id" uuid PRIMARY KEY,
  "name" varchar(255),
  "type" attribute_types
);

CREATE TABLE IF NOT EXISTS "position_attributes" (
  "id" uuid PRIMARY KEY,
  "attribute_id" uuid,
  "position_id" uuid,
  "value" varchar
);


ALTER TABLE "position" ADD CONSTRAINT  "position_profession_id_fkey"
 FOREIGN KEY ("profession_id") REFERENCES "profession" ("id") ON DELETE CASCADE;

ALTER TABLE "position_attributes" ADD CONSTRAINT  "position_attributes_attribute_id_fkey" 
FOREIGN KEY ("attribute_id") REFERENCES "attribute" ("id") ON DELETE CASCADE;

ALTER TABLE "position_attributes" ADD CONSTRAINT  "position_attributes_position_id_fkey"
 FOREIGN KEY ("position_id") REFERENCES "position" ("id") ON DELETE CASCADE;
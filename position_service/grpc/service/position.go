package service

import (
	"context"
	"position_service/config"
	pb "position_service/genproto/position_service"
	"position_service/pkg/logger"
	"position_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)


type positionService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedPositionServiceServer
}

func NewPositionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *positionService {
	return &positionService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *positionService) CreatePosition(ctx context.Context, req *pb.CreatePositionRequest )(resp *pb.PositionId,err error){
   
	resp,err = s.strg.Position().CreatePosition(ctx, req)
	if err != nil {
		s.log.Error("Create Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	
     return resp,err;

}

func (s *positionService) GetAllPosition(ctx context.Context, req *pb.GetAllPositionRequest)(*pb.GetAllPositionResponse,error){
    resp,err := s.strg.Position().GetAllPosition(ctx, req)
	if err != nil {
		s.log.Error("Create Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	
     return resp,err;

}

func (s *positionService) GetPosition(ctx context.Context,req *pb.PositionId)(resp *pb.Position,err error){
   
	resp,err = s.strg.Position().GetPosition(ctx, req)
	if err != nil {
		s.log.Error("Get Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return resp, nil
}


func (s *positionService) UpdatePosition(ctx context.Context,req *pb.UpdatePositionRequest) (*pb.PositionId,error){
  
	_,err:=s.strg.Position().UpdatePosition(ctx,req)
    if err != nil {
		s.log.Error("Update Position", logger.Any("req", req), logger.Error(err))
		return nil,err
	}
	
	 return  &pb.PositionId{
		Id:req.Id ,
	}, nil

}


func (s *positionService) DeletePosition(ctx context.Context,req *pb.PositionId) (*emptypb.Empty,error){
    err:=s.strg.Position().DeletePosition(ctx,req)
    if err != nil {
		s.log.Error("Delete Position", logger.Any("req", req), logger.Error(err))
		return &emptypb.Empty{},err
	}
	return  &emptypb.Empty{},err
}
package main

import (
	"Project6/api"
	"Project6/config"

	//_ "Project6/docs"
	"Project6/api/handlers"
	"Project6/storage/postgres"
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {

	cfg := config.Load()
	// fmt.Printf("%#+v\n", cfg)

	storage := postgres.NewPostgres(fmt.Sprintf("user=%s dbname=%s password=%s sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresDatabase,
		cfg.PostgresPassword,
		),
	)
	defer storage.CloseDB()
	h := handlers.NewHandler(storage,cfg)
	switch cfg.Environment {
	case "dev":
		gin.SetMode(gin.DebugMode)
	case "test":
		gin.SetMode(gin.TestMode)
	default:
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	api.SetUpAPI(r,h,cfg)

	r.Run(cfg.HTTPPort)
}

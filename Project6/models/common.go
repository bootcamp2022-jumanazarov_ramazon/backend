package models

type DefaultResponse struct{
	Message string  `json:"message"`
	Error   string   `json:"error"`
	Data interface{}  `json:"data"`
}


type QueryParams struct {
	Offset int `json:"offset" default:"0"`
	Limit int   `json:"limit" default:"10"`
	Search string `json:"search"`
}
package models

import (
	"time"

)
type Content struct {
	Title string  `json:"title" default:"Lorem"`
	Body  string  `json:"body"`
}


type Article struct {
	ID        string `json:"id"`
	Content          
	AuthorID   string    `json:"author_id"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

type CreateArticle struct{
	Content
	AuthorID   string    `json:"author_id"`
}
type UpdateArticle struct{
	Content
    AuthorID   string    `json:"author_id"`
}
type ArticleList struct {
	Articles []Article `json:"articles"`
	Count    int       `json:"count"`
}

type ArticleWithAuthor struct {
	ID        string `json:"id"`
	Content          
	Author     Person         `json:"author"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`

}
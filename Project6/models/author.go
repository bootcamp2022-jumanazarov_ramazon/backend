package models

import "time"

type Author struct{
	ID string `json:"id"`
	Person
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}


type CreateAuthorModel struct{
	Person
	
}

type UpdateAuthorModel struct{
	Person
}

type AuthorList struct {
	Authors []Author `json:"author"`
	Count    int       `json:"count"`
}



package storage

import (
	"Project6/models"
	//"Project6/storage/postgres"
	//"Project6/storage/inmemory"
)

type StorageI interface {
	Article() ArticleRepoI
	Author() AuthorRepoI
    CloseDB() error

}
type ArticleRepoI interface{
	 CreateArticle(entity models.Article) error 
     GetArticleList(queryParams models.QueryParams) (resp models.ArticleList, err error)
	 GetArticleByID(ID string) (val models.ArticleWithAuthor, err error)
	 UpdateArticle(ID string,entity models.Article) error
	 DeleteArticle(ID string) ( bool)

}

type AuthorRepoI interface{
	CreateAuthor(entity models.Author) error
	GetAuthorList(queryParams models.QueryParams) (resp models.AuthorList, err error)
	GetAuthorByID(ID string) (val models.Author, err error)
	UpdateAuthor(ID string, entity models.UpdateAuthorModel) error
	DeleteAuthor(ID string) bool

}
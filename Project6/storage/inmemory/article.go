package inmemory

import (
	"Project6/models"
	"errors"
	"strings"
	"time"

	
)

type articleRepoImpl struct{
   db map[string]models.Article
} 
var ArticleRepo = articleRepoImpl{}

func init() {
	ArticleRepo.db = make(map[string]models.Article)
}


func (r articleRepoImpl) CreateArticle(entity models.Article)error {
    _, ok := r.db[entity.ID]
    if ok {
		return errors.New("already exists")
	}
	now := time.Now()
	entity.CreatedAt=&now
	r.db[entity.ID]=entity
	return nil
}

func (r articleRepoImpl) GetArticleList(search string)(resp []models.Article ){
	    // to do filter result based on "search" query param
        
		for _, v := range r.db {
			if (strings.Contains(v.Content.Title,search)||strings.Contains(v.Content.Body,search)){
			resp = append(resp, v)
		}
	}
		return resp
}

func (r articleRepoImpl) GetArticleByID (ID string) (models.Article,error){
	val,ok := r.db[ID]
	if !ok {
		return val,errors.New("not found")
	}
	return val,nil
}

func (r articleRepoImpl) UpdateArticle(entity models.Article) error{
          val, ok := r.db[entity.ID]
		if !ok {
			return errors.New("not found")
		}
		now:=time.Now()
        val.Content = entity.Content
		val.AuthorID = entity.AuthorID
		val.UpdatedAt=&now
        
		r.db[val.ID] = val
		return nil
}

func (r articleRepoImpl) DeleteArticle(ID string)error{
	 _, ok := r.db[ID]
		if !ok {
			return errors.New("not found")
		}
	delete(r.db,ID)
	return nil	
}
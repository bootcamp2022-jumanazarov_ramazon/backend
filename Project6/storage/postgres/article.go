package postgres

import (
	"Project6/models"
	"github.com/google/uuid"
    "errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type articleRepoImpl struct {
	db *sqlx.DB
}

func (r articleRepoImpl) CreateArticle(entity models.Article) error {

	createArticleQuery := `INSERT INTO "article" ("id", "title", "body", "author_id") VALUES ($1, $2, $3, $4)`
	result, err := r.db.Exec(createArticleQuery, uuid.New(), entity.Title, entity.Body, entity.AuthorID)
	if err != nil {
		return err
	}
	fmt.Println(result.RowsAffected())

	return nil
}

func (r articleRepoImpl) GetArticleList(queryParams models.QueryParams) (resp models.ArticleList, err error) {
	resp.Articles = []models.Article{}

	params := make(map[string]interface{})
	query := `SELECT
		id,
		title,
		body,
		author_id,
		created_at,
		updated_at
		FROM article
		`
	filter := " WHERE true"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if len(queryParams.Search) > 0 {
		params["search"] = queryParams.Search
		filter += " AND ((title ILIKE '%' || :search || '%') OR (body ILIKE '%' || :search || '%'))"
	}

	if queryParams.Offset > 0 {
		params["offset"] = queryParams.Offset
		offset = " OFFSET :offset"
	}

	if queryParams.Limit > 0 {
		params["limit"] = queryParams.Limit
		limit = " LIMIT :limit"
	}

	cQ := "SELECT count(1) FROM article" + filter
	row, err := r.db.NamedQuery(cQ, params)
	if err != nil {
		return resp, err
	}
	defer row.Close()

	if row.Next() {
		err = row.Scan(
			&resp.Count,
		)
		if err != nil {
			return resp, err
		}
	}

	q := query + filter + offset + limit
	rows, err := r.db.NamedQuery(q, params)
	if err != nil {
		return resp, err
	}
	defer rows.Close()
	for rows.Next() {
		var e models.Article

		err = rows.Scan(
			&e.ID,
			&e.Title,
			&e.Body,
			&e.AuthorID,
			&e.CreatedAt,
			&e.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Articles = append(resp.Articles, e)
	}

	return resp, nil
}
func (r articleRepoImpl) GetArticleByID(ID string) (val models.ArticleWithAuthor, err error) {
	query:=`SELECT 
	 ar.id,
	 ar.title,
	 ar.body,
	 au.firstname,
	 au.lastname,
	 ar.created_at,
	 ar.updated_at
	 FROM article as ar JOIN author as au ON ar.author_id=au.id
	WHERE ar.id = $1`
	id, err := uuid.Parse(ID)
	if err != nil {
		return val, err
	}
    row := r.db.QueryRow(query, id)

	err = row.Scan(
			&val.ID,
			&val.Title,
			&val.Body,
			&val.Author.Firstname,
			&val.Author.Lastname,
			&val.CreatedAt,
			&val.UpdatedAt,
		)
	if err != nil {
		return val, err
	}
	return val, nil
}

func (r articleRepoImpl) UpdateArticle(ID string,entity models.Article) error {
	 id, err := uuid.Parse(ID)
	if err != nil {
		return err
	}

     updateArticleQuery:=`UPDATE article SET 
	 "title"=$1, "body"=$2, "author_id"=$3,"updated_at"=NOW()
	 WHERE id = $4`
	 res, err := r.db.Exec(updateArticleQuery, entity.Title, entity.Body,entity.AuthorID,id)
	if err != nil{
		return err
	}
       count, err := res.RowsAffected()  
       if err == nil && count==0{
			return errors.New("article not found")
		  }
	return nil
}

func (r articleRepoImpl) DeleteArticle(ID string) ( bool) {
	 
	 id, err := uuid.Parse(ID)
	if err != nil {
		return false
	}
     query:=`DELETE FROM article WHERE id = $1`
	res,err := r.db.Exec(query,id)

    if err == nil {

       count, err := res.RowsAffected()  
       if err == nil && count!=0{
			return true
		  }
    }
    return false
}

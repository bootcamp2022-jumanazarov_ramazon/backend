package postgres

import (
	"Project6/models"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type authorRepoImpl struct {
	db *sqlx.DB
}

func (r authorRepoImpl) CreateAuthor(entity models.Author) error {

	createAuthorQuery := `INSERT INTO "author" ("id", "firstname", "lastname") VALUES ($1, $2, $3)`

	result, err := r.db.Exec(createAuthorQuery, uuid.New(), entity.Firstname, entity.Lastname)
	if err != nil {
		return err
	}

	fmt.Println(result.RowsAffected())
	return nil
}

func (r authorRepoImpl) GetAuthorList(queryParams models.QueryParams) (resp models.AuthorList, err error) {
	resp.Authors = []models.Author{}

	params := make(map[string]interface{})
	query := `SELECT
		id,
		firstname,
		lastname,
		created_at,
		updated_at
		FROM author
		`
	filter := " WHERE true"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if len(queryParams.Search) > 0 {
		params["search"] = queryParams.Search
		filter += " AND ((firstname ILIKE '%' || :search || '%') OR (lastname ILIKE '%' || :search || '%'))"
	}

	if queryParams.Offset > 0 {
		params["offset"] = queryParams.Offset
		offset = " OFFSET :offset"
	}

	if queryParams.Limit > 0 {
		params["limit"] = queryParams.Limit
		limit = " LIMIT :limit"
	}

	cQ := "SELECT count(1) FROM author" + filter
	row, err := r.db.NamedQuery(cQ, params)
	if err != nil {
		return resp, err
	}
	defer row.Close()

	if row.Next() {
		err = row.Scan(
			&resp.Count,
		)
		if err != nil {
			return resp, err
		}
	}

	q := query + filter + offset + limit
	rows, err := r.db.NamedQuery(q, params)
	if err != nil {
		return resp, err
	}
	defer rows.Close()
	for rows.Next() {
		var e models.Author

		err = rows.Scan(
			&e.ID,
			&e.Firstname,
			&e.Lastname,
			&e.CreatedAt,
			&e.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Authors = append(resp.Authors, e)
	}

	return resp, nil
}
func (r authorRepoImpl) GetAuthorByID(ID string) (val models.Author, err error) {
	query := `SELECT id as id,
	 firstname as "author.firstname",
	 lastname as "author.lastname",
	 created_at as createdAt,
	 updated_at as updatedAt 
	 FROM author 
	WHERE id = $1`
	id, err := uuid.Parse(ID)
	if err != nil {
		return val, err
	}
	row := r.db.QueryRow(query, id)

	err = row.Scan(
			&val.ID,
			&val.Firstname,
			&val.Lastname,
			&val.CreatedAt,
			&val.UpdatedAt,
		)
	if err != nil {
		return val, err
	}

	return val, nil
}

func (r authorRepoImpl) UpdateAuthor(ID string, entity models.UpdateAuthorModel) error {
	id, err := uuid.Parse(ID)
	if err != nil {
		return err
	}

	updateArticleQuery := `UPDATE author SET 
	 "firstname"=$1, "lastname"=$2,"updated_at"=NOW()
	 WHERE id = $3`
	res, err := r.db.Exec(updateArticleQuery, entity.Firstname, entity.Lastname, id)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err == nil && count == 0 {
		return errors.New("author not found")
	}
	return nil
}

func (r authorRepoImpl) DeleteAuthor(ID string) bool {

	id, err := uuid.Parse(ID)
	if err != nil {
		return false
	}
	query := `DELETE FROM author WHERE id = $1`
	res, err := r.db.Exec(query, id)

	if err == nil {

		count, err := res.RowsAffected()
		if err == nil && count != 0 {
			return true
		}
	}
	return false
}

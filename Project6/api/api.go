package api

import (
	"Project6/config"
	"Project6/api/docs"
    "github.com/swaggo/files"
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
	"github.com/gin-gonic/gin"
	"Project6/api/handlers"
)


// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/
// @contact.name    API Support
// @contact.url     http://www.swagger.io/support
// @contact.email   support@swagger.io
// @license.name    Apache 2.0
// @license.url     http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath        /api/v1



func SetUpAPI(r *gin.Engine, h handlers.HandlerImpl,cfg config.Config){

    docs.SwaggerInfo.Title = cfg.ProjectName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{"http", "https"}


	r.GET("/ping", h.Ping)

	api := r.Group("/api")
	v1 := api.Group("v1")
	{
		v1.GET("/articles", h.GetArticleList)
		v1.GET("/articles/:id", h.GetArticleByID)
		v1.POST("/articles", h.CreateArticle)
		v1.PUT("/articles/:id", h.UpdateArticle)
		v1.DELETE("/articles/:id", h.DeleteArticle)

		v1.GET("/authors", h.GetAuthorList)
		v1.GET("/authors/:id", h.GetAuthorByID)
		v1.POST("/authors", h.CreateAuthor)
		v1.PUT("/authors/:id", h.UpdateAuthor)
		v1.DELETE("/authors/:id", h.DeleteAuthor)
	}

	r.StaticFS("/static", gin.Dir("static", false))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}
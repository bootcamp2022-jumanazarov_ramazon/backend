package handlers

import (
	"Project6/models"
	//"Project6/storage"
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	//"strconv"
)

// ShowAccount   GetArticleList
// @ID           get-article-list
// @Summary      Get Article List
// @Description  Get Article List based on query params
// @Tags         article 
// @Accept       json
// @Produce      json
// @Param        search  query     string                                           false  "input search text"  
// @Param        offset  query     int                                              false  "offset"             
// @Param        limit   query     int                                              false  "limit"              
// @Success      200     {object}  models.DefaultResponse{data=models.ArticleList}  "Success Response"
// @Success      400     {object}  models.DefaultResponse                           "Bad Request Response"
// @Success      500     {object}  models.DefaultResponse                           "Internal Server Error Response"
// @Router       /articles [GET]
func (h *HandlerImpl) GetArticleList(c *gin.Context) {
	search := c.Query("search")

	offset, err := h.parseOffsetQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	limit, err := h.parseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp, err := h.strg.Article().GetArticleList(models.QueryParams{
		Search: search,
		Offset: offset,
		Limit:  limit,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "GetArticleList",
		"data":    resp,
	})
}

// ShowAccount   GetArticleByID
// @ID           get-article-by-id
// @Summary      Get Article By ID
// @Description  Get Article based on ID
// @Tags         article 
// @Accept       json
// @Produce      json
// @Param        article_id  path      string                  true  "Article ID"
// @Success      200         {object}  models.DefaultResponse{data=models.Article}  "Success Response"
// @Success      404         {object}  models.DefaultResponse  "Not Found Response"
// @Router       /articles/{article_id} [GET]
func (h *HandlerImpl)GetArticleByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Article().GetArticleByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "GetArticleByID",
		"article":resp,
	})
}

// ShowAccount   CreateArticle
// @ID           create-article
// @Summary      Create an article
// @Description  Create an article based on given body
// @Tags         article
// @Accept       json
// @Produce      json
// @Param        article     body      models.CreateArticle    true  "article body"
// @Success      201         {object}  models.DefaultResponse  "Success Response"
// @Success      400         {object}  models.DefaultResponse  "Bad Request Response"
// @Success      500      {object}  models.DefaultResponse  "Internal Server Error Response"
// @Router       /articles [POST]
func (h *HandlerImpl) CreateArticle(c *gin.Context) {
	var data models.CreateArticle
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := h.strg.Article().CreateArticle(models.Article{ 
		Content: data.Content,
		AuthorID:  data.AuthorID,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "Article has been created",
		"data":nil,
	})
}

// ShowAccount   DeleteArticle
// @ID           delete-article
// @Summary      Delete an Article
// @Description  Delete Article based on ID
// @Tags         article 
// @Accept       json
// @Produce      json
// @Param        article_id  path      string                  true  "Article ID"
// @Success      200         {object}  models.DefaultResponse  "Success Response"
// @Success      404         {object}  models.DefaultResponse                       "Not Found Response"
// @Router       /articles/{article_id} [DELETE]
func (h *HandlerImpl) DeleteArticle(c *gin.Context) {
	id := c.Param("id")
	result := h.strg.Article().DeleteArticle(id)
	if !result  {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Article has been deleted",
	})
}

// ShowAccount   UpdateArticle
// @ID           update-article
// @Summary      Update an article
// @Description  Update an article based on given body
// @Tags         article
// @Accept       json
// @Produce      json
// @Param        article_id  path      string                                       true  "Article ID"
// @Param        article  body      models.CreateArticle    true  "article body"
// @Success      201      {object}  models.DefaultResponse  "Success Response"
// @Success      400      {object}  models.DefaultResponse  "Bad Request Response"
// @Success      404         {object}  models.DefaultResponse  "Not Found Response"
// @Router       /articles/{article_id} [PUT]
func (h *HandlerImpl) UpdateArticle(c *gin.Context) {
	id := c.Param("id")
	var data models.UpdateArticle
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	fmt.Print(data)
	err := h.strg.Article().UpdateArticle(id,models.Article{
		Content:   data.Content,
		AuthorID:    data.AuthorID,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Article has been updated",
	})
}

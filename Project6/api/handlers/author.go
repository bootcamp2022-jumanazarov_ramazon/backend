package handlers

import (
	"Project6/models"
	//"Project6/storage"
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	//"strconv"
)

// ShowAccount   GetAuthorList
// @ID           get-author-list
// @Summary      Get Author List
// @Description  Get Author List based on query params
// @Tags         author 
// @Accept       json
// @Produce      json
// @Param        search  query     string                                          false  "input search text"  
// @Param        offset  query     int                                             false  "offset"             
// @Param        limit   query     int                                             false  "limit"              
// @Success      200     {object}  models.DefaultResponse{data=models.AuthorList}  "Success Response"
// @Success      400     {object}  models.DefaultResponse                          "Bad Request Response"
// @Success      500     {object}  models.DefaultResponse                          "Internal Server Error Response"
// @Router       /authors [GET]
func (h *HandlerImpl)GetAuthorList(c *gin.Context) {
	search := c.Query("search")

	offset, err := h.parseOffsetQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	limit, err := h.parseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp, err := h.strg.Author().GetAuthorList(models.QueryParams{
		Search: search,
		Offset: offset,
		Limit:  limit,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "GetAuthorList",
		"data":    resp,
	})
}

// ShowAccount   GetAuthorByID
// @ID           get-author-by-id
// @Summary      Get Author By ID
// @Description  Get Author based on ID
// @Tags         author 
// @Accept       json
// @Produce      json
// @Param        author_id  path      string                                      true  "Author ID"
// @Success      200        {object}  models.DefaultResponse{data=models.Author}  "Success Response"
// @Success      404        {object}  models.DefaultResponse                      "Not Found Response"
// @Router       /authors/{author_id} [GET]
func (h *HandlerImpl) GetAuthorByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Author().GetAuthorByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "GetAuthorByID",
		"article":resp,
	})
}

// ShowAccount   CreateAuthor
// @ID           create-author
// @Summary      Create an author
// @Description  Create an author based on given body
// @Tags         author
// @Accept       json
// @Produce      json
// @Param        author  body      models.CreateAuthorModel  true  "author body"
// @Success      201     {object}  models.DefaultResponse    "Success Response"
// @Success      400     {object}  models.DefaultResponse    "Bad Request Response"
// @Success      500     {object}  models.DefaultResponse    "Internal Server Error Response"
// @Router       /authors [POST]
func (h *HandlerImpl) CreateAuthor(c *gin.Context) {
	var data models.CreateAuthorModel
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := h.strg.Author().CreateAuthor(models.Author{ 

		Person:data.Person,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"message": "Author has been created",
		"data":nil,
	})
}

// ShowAccount   DeleteAuthor
// @ID           delete-author
// @Summary      Delete an Author
// @Description  Delete Author based on ID
// @Tags         author 
// @Accept       json
// @Produce      json
// @Param        author_id  path      string                  true  "Author ID"
// @Success      200        {object}  models.DefaultResponse  "Success Response"
// @Success      404        {object}  models.DefaultResponse  "Not Found Response"
// @Router       /authors/{author_id} [DELETE]
func (h *HandlerImpl) DeleteAuthor(c *gin.Context) {
	id := c.Param("id")
	result := h.strg.Author().DeleteAuthor(id)
	if !result  {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "not found",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Author has been deleted",
	})
}

// ShowAccount   UpdateAuthor
// @ID           update-author
// @Summary      Update an author
// @Description  Update an author based on given body
// @Tags         author
// @Accept       json
// @Produce      json
// @Param        author_id  path      string                    true  "Author ID"
// @Param        author     body      models.UpdateAuthorModel  true  "author body"
// @Success      201        {object}  models.DefaultResponse    "Success Response"
// @Success      400        {object}  models.DefaultResponse    "Bad Request Response"
// @Success      404        {object}  models.DefaultResponse    "Not Found Response"
// @Router       /authors/{author_id} [PUT]
func (h *HandlerImpl) UpdateAuthor(c *gin.Context) {
	id := c.Param("id")
	var data models.UpdateAuthorModel
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	fmt.Print(data)
	err := h.strg.Author().UpdateAuthor(id,models.UpdateAuthorModel{
		Person:data.Person,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Author has been updated",
	})
}